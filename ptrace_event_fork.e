note
	description: "A {PTRACE_EVENT} for a new forked process."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Thu, 03 Feb 2022 21:42:45 +0000"
	revision:    "0.1"

class
	PTRACE_EVENT_FORK

inherit
	PTRACE_EVENT

create {PTRACE}
	make_from_event

feature {NONE} -- Initialisation

	make_from_event(a_event:PTRACE_EVENT; a_pid:INTEGER)
			-- Initialisation of `Current' using `a_event' as a fork (fork, vfork, clone or vfork_done)
			-- event and using `a_pid' as `pid'.
		require
			Is_Fork_Event: a_event.is_fork or a_event.is_vfork or a_event.is_clone or a_event.is_vfork_done
		do
			item := a_event.item
			pid := a_pid
		ensure
			Is_Item_Assign: item ~ a_event.item
			Is_Pid_Assign: pid ~ a_pid
		end

feature -- Access

	pid:INTEGER
			-- The process ID of the forked process

invariant
	Is_Fork_Event: is_fork or is_vfork or is_vfork_done or is_clone
end
