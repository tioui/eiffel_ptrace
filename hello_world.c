/**
 * @author      : Louis Marchand (prog@tioui.com)
 * @created     : samedi jan 01, 2022 10:37:10 EST
 * @license     : MIT
 */

#include <stdio.h>
#include <signal.h>

int main(int param_count, char *params[]) {
	printf("Hello World 1!\n");
	raise(SIGTRAP);
	printf("Hello World 2!\n");
	return 0;
}
