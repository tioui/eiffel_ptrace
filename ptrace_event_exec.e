note
	description: "A {PTRACE_EVENT} for a new thread."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Thu, 03 Feb 2022 21:42:45 +0000"
	revision:    "0.1"

class
	PTRACE_EVENT_EXEC

inherit
	PTRACE_EVENT

create {PTRACE}
	make_from_event

feature {NONE} -- Initialisation

	make_from_event(a_event:PTRACE_EVENT; a_thread_id:INTEGER)
			-- Initialisation of `Current' using `a_event' as an exec
			-- event and using `a_thread_id' as `thread_id'.
		require
			Is_Exec_Event: a_event.is_exec
		do
			item := a_event.item
			thread_id := a_thread_id
		ensure
			Is_Item_Assign: item ~ a_event.item
			Is_Thread_Id_Assign: thread_id ~ a_thread_id
		end

feature -- Access

	thread_id:INTEGER
			-- The thread ID of the new thread

invariant
	Is_Exec_Event: is_exec
end
