note
	description: "A signal information code."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"
	ToDo:        "PTRACE_SIGNAL_CODE_ILL, PTRACE_SIGNAL_CODE_FPE, PTRACE_SIGNAL_CODE_SEGV, PTRACE_SIGNAL_CODE_BUS, PTRACE_SIGNAL_CODE_TRAP, PTRACE_SIGNAL_CODE_CHILD, PTRACE_SIGNAL_CODE_IO"

class
	PTRACE_SIGNAL_CODE

create {PTRACE_SIGNAL_INFORMATION}
	make

create
	make_kill,
	make_kernel,
	make_queue,
	make_timer,
	make_message_queue,
	make_async_io,
	make_io,
	make_tkill

feature {NONE} -- Initialisation

	make(a_item:INTEGER)
			-- Initialisation of `Current' using `a_item' as `item'
		do
			item := a_item
		ensure
			Is_Item_Assign: item ~ a_item
		end


	make_kill
			-- Initialisation of `Current' as `is_kill'
		do
			set_kill
		ensure
			Is_Set: is_kill
		end

	make_kernel
			-- Initialisation of `Current' as `is_kernel'
		do
			set_kernel
		ensure
			Is_Set: is_kernel
		end

	make_queue
			-- Initialisation of `Current' as `is_queue'
		do
			set_queue
		ensure
			Is_Assign: is_queue
		end

	make_timer
			-- Initialisation of `Current' as `is_timer'
		do
			set_timer
		ensure
			Is_Assign: is_timer
		end

	make_message_queue
			-- Initialisation of `Current' as `is_message_queue'
		do
			set_message_queue
		ensure
			Is_Assign: is_message_queue
		end

	make_async_io
			-- Initialisation of `Current' as `is_async_io'
		do
			set_async_io
		ensure
			Is_Assign: is_async_io
		end

	make_io
			-- Initialisation of `Current' as `is_io'
		do
			set_io
		ensure
			Is_Assign: is_io
		end

	make_tkill
			-- Initialisation of `Current' as `is_tkill'
		do
			set_tkill
		ensure
			Is_Assign: is_tkill
		end


feature -- Access

	is_kill:BOOLEAN
			-- The signal was send by 'kill'
		do
			Result := item ~ c_si_user
		end

	is_kernel:BOOLEAN
			-- The signal was send by the kernel
		do
			Result := item ~ c_si_kernel
		end

	is_queue:BOOLEAN
			-- The signal was send by 'sigqueue'
		do
			Result := item ~ c_si_queue
		end

	is_timer:BOOLEAN
			-- The signal send when the POSIX timer expired
		do
			Result := item ~ c_si_timer
		end

	is_message_queue:BOOLEAN
			-- POSIX message queue state changed (since Linux 2.6.6)
		do
			Result := item ~ c_si_mesgq
		end

	is_async_io:BOOLEAN
			-- Asynchronous IO (AIO) completed
		do
			Result := item ~ c_si_asyncio
		end

	is_io:BOOLEAN
			-- IO signal (SIGIO) queued
			-- (only in kernels up to Linux 2.2; look at PTRACE_SIGNAL_CODE_IO for
			-- Linux 2.4 and onward)
		do
			Result := item ~ c_si_sigio
		end

	is_tkill:BOOLEAN
			-- Send by 'tkill' or 'tgkill' (since Linux 2.4.19)
		do
			Result := item ~ c_si_tkill
		end

feature -- Status setting

	set_kill
			-- Enable `is_kill'
		do
			item := c_si_user
		ensure
			Is_Set: is_kill
		end

	set_kernel
			-- Enable `is_kernel'
		do
			Item := c_si_kernel
		ensure
			Is_Set: is_kernel
		end

	set_queue
			-- Enable `is_queue'
		do
			item := c_si_queue
		ensure
			Is_Assign: is_queue
		end

	set_timer
			-- Enable `is_timer'
		do
			item := c_si_timer
		ensure
			Is_Assign: is_timer
		end

	set_message_queue
			-- Enable `is_message_queue'
		do
			item := c_si_mesgq
		ensure
			Is_Assign: is_message_queue
		end

	set_async_io
			-- Enable `is_async_io'
		do
			item := c_si_asyncio
		ensure
			Is_Assign: is_async_io
		end

	set_io
			-- Enable `is_io'
		do
			item := c_si_sigio
		ensure
			Is_Assign: is_io
		end

	set_tkill
			-- Enable `is_tkill'
		do
			item := c_si_tkill
		ensure
			Is_Assign: is_tkill
		end



feature {NONE} -- Implementation

	item:INTEGER
			-- Internal reprensentation of `Current'

feature {NONE} -- External

	c_si_user:INTEGER
			-- Signal send by 'kill'
		external
			"C inline use <signal.h>"
		alias
			"SI_USER"
		end

	c_si_kernel:INTEGER
			-- Signal send by the kernel
		external
			"C inline use <signal.h>"
		alias
			"SI_KERNEL"
		end

	c_si_queue:INTEGER
			-- Signal send by 'sigqueue'
		external
			"C inline use <signal.h>"
		alias
			"SI_QUEUE"
		end

	c_si_timer:INTEGER
			-- Signal send when the POSIX timer expired
		external
			"C inline use <signal.h>"
		alias
			"SI_TIMER"
		end

	c_si_mesgq:INTEGER
			-- POSIX message queue state changed (since Linux 2.6.6)
		external
			"C inline use <signal.h>"
		alias
			"SI_MESGQ"
		end

	c_si_asyncio:INTEGER
			-- Asynchronous IO (AIO) completed
		external
			"C inline use <signal.h>"
		alias
			"SI_ASYNCIO"
		end

	c_si_sigio:INTEGER
			-- Queued SIGIO (only in kernels up to Linux 2.2)
		external
			"C inline use <signal.h>"
		alias
			"SI_SIGIO"
		end

	c_si_tkill:INTEGER
			-- Send by 'tkill' or 'tgkill' (since Linux 2.4.19)
		external
			"C inline use <signal.h>"
		alias
			"SI_TKILL"
		end




end
