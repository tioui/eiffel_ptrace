note
	description: "Use to trace a program execution."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"

deferred class
	PTRACE

inherit
	ANY
		redefine
			default_create
		end

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			set_options (create {PTRACE_OPTIONS})
		end

feature -- Access

	process_wait_signal:detachable PTRACE_SIGNAL_READABLE
			-- The signal returned by the last call to `wait_process', if any

	process_wait_event:detachable PTRACE_EVENT
			-- The event returned by the last call to `wait_process', if any.
			-- May be a simple {PTRACE_EVENT}, a {PTRACE_EVENT_EXIT},
			-- a {PTRACE_EVENT_EXEC} or a {PTRACE_EVENT_FORK}.

	wait_process(a_options:PTRACE_WAIT_PROCESS_OPTIONS)
			-- Wait for an event to appen in the process that is traced.
		require
			Is_Tracing: is_tracing
		local
			l_status, l_result_pid, l_message:INTEGER
			l_process_wait_event:PTRACE_EVENT
		do
			l_result_pid := c_waitpid (pid, $l_status, a_options.item)
			check PID_Valid: l_result_pid ~ pid end
			if {PTRACE_SIGNAL_READABLE}.is_signal_status(l_status) then
				create process_wait_signal.make_from_status(l_status)
			else
				process_wait_signal := Void
			end
			create l_process_wait_event.make (l_status)
			if l_process_wait_event.is_exit then
				c_get_event_message(pid, $l_message)
				create {PTRACE_EVENT_EXIT}l_process_wait_event.make_from_event (l_process_wait_event, l_message)
			elseif l_process_wait_event.is_exec then
				c_get_event_message(pid, $l_message)
				create {PTRACE_EVENT_EXEC}l_process_wait_event.make_from_event (l_process_wait_event, l_message)
			elseif
				l_process_wait_event.is_clone or l_process_wait_event.is_fork
			or
				l_process_wait_event.is_vfork or l_process_wait_event.is_vfork_done
			then
				c_get_event_message(pid, $l_message)
				create {PTRACE_EVENT_FORK}l_process_wait_event.make_from_event (l_process_wait_event, l_message)
			elseif l_process_wait_event.is_unused then
				l_process_wait_event := Void
			end
			process_wait_event := l_process_wait_event
		end


	read_data(a_address:NATURAL):INTEGER
			-- Read a word of the tracee's data memory at address `a_address'.
			-- Note: on Linux, `read_word' and `read_text' are equivalent.
		require
			Address_not_null: a_address /= 0
		do
			Result := c_peekdata(pid, a_address)
		end

	read_text(a_address:NATURAL):INTEGER
			-- Read a word of the tracee's text memory at address `a_address'.
			-- Note: on Linux, `read_word' and `read_text' are equivalent.
		require
			Address_not_null: a_address /= 0
		do
			Result := c_peektext(pid, a_address)
		end

	read_user(a_address:NATURAL):INTEGER
			-- Read a word of the tracee's user memory at address `a_address'.
		require
			Address_not_null: a_address /= 0
		do
			Result := c_peekuser(pid, a_address)
		end



	-- TODO: PTRACE_GETREGS, PTRACE_GETFPREGS, PTRACE_GETREGSET
	
	get_registers:PTRACE_REGISTER
		do
			create Result
		end

	signal_information:PTRACE_SIGNAL_INFORMATION
			-- The information about the signal that have stop the tracee's process.
			-- Note: On SPARK system, `signal_information_spark' must be use.
		do
			create Result
			if Result.exists then
				c_get_info(pid, Result.item)
			end
		end

	signal_information_sparc:PTRACE_SIGNAL_INFORMATION
			-- The information about the signal that have stop the tracee's process.
			-- Only use in SPARC system
		do
			create Result
			if Result.exists then
				c_get_info_sparc(pid, Result.item)
			end
		end

	options:PTRACE_OPTIONS assign set_options
			-- The options used in `Current'

	is_tracing:BOOLEAN
			-- Used when `Current' is tracing a program

	pid:INTEGER
			-- The ID of the process that is been traced.
			-- Set to 0 until the process is running.

	error_code:INTEGER
			-- Represent witch error has been detected. 0 if no error found.

	has_error:BOOLEAN
			-- There was an error in `Current'
		do
			Result := error_code /= 0
		end

	has_busy_error:BOOLEAN
			-- There was an error with allocating or freeing a debug register (in i386 only)
		do
			Result := error_code ~ c_ebusy
		end

	has_fault_error:BOOLEAN
			-- There was an attempt to read from or write to an invalid area in the tracer's
			-- or the tracee's memory, probably because the area wasn't mapped or accessible.
			-- On Linux, sometime, an `has_invalid_request_error' will be return instead.
		do
			Result := error_code ~ c_efault
		end

	has_invalid_option_error:BOOLEAN
			-- An attempt was made to set an invalid option.
		do
			Result := error_code ~ c_einval
		end

	has_invalid_request_error:BOOLEAN
			-- An attempt was made to set an invalid option.
			-- Can also be an attempt was made to read from or write
			-- to an invalid area in the tracer's or the tracee's memory;
			-- or there was a word-alignment violation;
			-- or an invalid signal was specified during a restart request.
		do
			Result := error_code ~ c_eio
		end

	has_invalid_permission_error:BOOLEAN
			-- The specified process cannot be traced.
			-- This could be because the tracer has insufficient privileges
			-- (the required capability is CAP_SYS_PTRACE); unprivileged processes
			-- cannot trace processes that they cannot send signals to or those running
			-- set-user-ID/set-group-ID programs, for obvious reasons. Alternatively,
			-- the process may already be being traced.
		do
			Result := error_code ~ c_eperm
		end

	has_search_error:BOOLEAN
			-- The specified process does not exist, or is not currently being traced
			-- by the caller, or is not stopped (for requests that require a stopped tracee).
		do
			Result := error_code ~ c_esrch
		end

feature -- Setting

	put_data(a_address:NATURAL; a_word:INTEGER)
			-- Copy `a_word' in the tracee's data memory at address `a_address'.
			-- Note: on Linux, `put_word' and `put_text' are equivalent.
		require
			Address_not_null: a_address /= 0
		do
			c_pokedata(pid, a_address, a_word)
		ensure
			Is_Copied: read_data(a_address) ~ a_word
		end

	put_text(a_address:NATURAL; a_word:INTEGER)
			-- Copy `a_word' in the tracee's text memory at address `a_address'.
			-- Note: on Linux, `put_word' and `put_text' are equivalent.
		require
			Address_not_null: a_address /= 0
		do
			c_pokedata(pid, a_address, a_word)
		ensure
			Is_Copied: read_text(a_address) ~ a_word
		end

	put_user(a_address:NATURAL; a_word:INTEGER)
			-- Copy `a_word' in the tracee's user memory at address `a_address'.
			-- Note that to keep the integrity of the kernel, some modifications
			-- of the user space are prohibited.
		require
			Address_not_null: a_address /= 0
		do
			c_pokedata(pid, a_address, a_word)
		end

	-- TODO: PTRACE_SETREGS, PTRACE_SETFPREGS, PTRACE_SETREGSET

	put_signal_information(a_information: PTRACE_SIGNAL_INFORMATION)
			-- Copy `a_information' to the tracee process as signal information.
		require
			Information_Exists: a_information.exists
		do
			c_set_info(pid, a_information.item)
		end

	set_options(a_options:PTRACE_OPTIONS)
			-- Assign `options' with the value of `a_options'
		do
			c_set_options(pid, a_options.internal_value)
			options := a_options.twin
		ensure
			Is_Assign: options ~ a_options
		end

feature {NONE} -- Externals

	c_ebusy:INTEGER
			-- Error code for an error with allocating or freeing a debug register (in i386 only)
		external
			"C inline use <sys/ptrace.h>"
		alias
			"EBUSY"
		end

	c_efault:INTEGER
			-- Error code for an attempt to read from or write to an invalid area.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"EFAULT"
		end

	c_einval:INTEGER
			-- Error code for an attempt was made to set an invalid option.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"EINVAL"
		end

	c_eio:INTEGER
			-- Error code for an invalid request
		external
			"C inline use <sys/ptrace.h>"
		alias
			"EIO"
		end

	c_eperm:INTEGER
			-- The specified process cannot be traced
		external
			"C inline use <sys/ptrace.h>"
		alias
			"EPERM"
		end

	c_esrch:INTEGER
			-- The specified process does not exist, or is not currently being
			-- traced by the caller, or is not stopped.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ESRCH"
		end

	c_peekdata(a_pid:INTEGER; a_address:NATURAL):INTEGER
			-- Read a word at the address `a_address' in the tracee's data memory,
			-- returning the word as the result
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_PEEKDATA, $a_pid, $a_address, 0)"
		end

	c_peektext(a_pid:INTEGER; a_address:NATURAL):INTEGER
			-- Read a word at the address `a_address' in the tracee's text memory,
			-- returning the word as the result
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_PEEKTEXT, $a_pid, $a_address, 0)"
		end

	c_peekuser(a_pid:INTEGER; a_address:NATURAL):INTEGER
			-- Read a word at the address `a_address' in the tracee's user memory,
			-- returning the word as the result
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_PEEKUSER, $a_pid, $a_address, 0)"
		end

	c_pokedata(a_pid:INTEGER; a_address:NATURAL; a_word:INTEGER)
			-- Put `a_word' at the address `a_address' in the tracee's data memory.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_POKEDATA, $a_pid, $a_address, 0)"
		end

	c_poketext(a_pid:INTEGER; a_address:NATURAL; a_word:INTEGER)
			-- Put `a_word' at the address `a_address' in the tracee's text memory.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_POKETEXT, $a_pid, $a_address, 0)"
		end

	c_pokeuser(a_pid:INTEGER; a_address:NATURAL; a_word:INTEGER)
			-- Put `a_word' at the address `a_address' in the tracee's user memory.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_POKEUSER, $a_pid, $a_address, 0)"
		end

	c_get_info(a_pid:INTEGER; a_address:POINTER)
			-- Retrieve information about the signal that caused the stop.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_GETSIGINFO, $a_pid, 0, $a_address)"
		end

	c_get_info_sparc(a_pid:INTEGER; a_address:POINTER)
			-- Retrieve information about the signal that caused the stop (SPARC version).
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_GETSIGINFO, $a_pid, $a_address, 0)"
		end

	c_set_info(a_pid:INTEGER; a_address:POINTER)
			-- Set signal information.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_SETSIGINFO, $a_pid, 0, $a_address)"
		end

	c_set_options(a_pid:INTEGER; a_option:INTEGER)
			-- Set ptrace options from `a_option'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_SETOPTIONS, $a_pid, 0, $a_option)"
		end

	c_get_event_message(a_pid:INTEGER; a_address:POINTER)
			-- Get the event message and put it in `a_address'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_GETEVENTMSG, $a_pid, 0, $a_address)"
		end

	c_waitpid(a_pid:INTEGER; a_status:POINTER; a_options:INTEGER):INTEGER
			-- Wait for process to change state and return the PID of the process.
			-- `a_pid' can be -1 (for every child process), 0 (for current process),
			-- < -1 (for process child in group ID is equal to the absolute value of `a_pid')
			-- or > 1 (for process child qith id equal `a_pid').
		external
			"C inline use <sys/wait.h>, <sys/types.h>"
		alias
			"waitpid($a_pid, $a_status, $a_options)"
		end

invariant

	PID_Valid: is_tracing implies pid /= 0

end
