note
	description: "A {PTRACE} that start a new process from a program file."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"

class
	PTRACE_FILE

inherit
	PTRACE

create
	make

feature -- Instance Free routines

	is_file_exists_and_readable(a_filename:READABLE_STRING_GENERAL):BOOLEAN
			-- True of the file at `a_filename' exists and is readable
		require -- from FILE
			filename_attached: a_filename /= Void
			filename_not_empty: not a_filename.is_empty
		local
			l_file:RAW_FILE
		do
			create l_file.make_with_name (a_filename)
			Result := l_file.exists and l_file.is_access_readable
		ensure
			class
		end

feature {NONE} -- Initialisation

	make(a_filename:READABLE_STRING_GENERAL; a_arguments:ITERABLE[READABLE_STRING_GENERAL])
			-- Create a trace using the program at `a_filename' and
			-- sending `a_argument' as program arguments.
			-- Note that the `a_filename' should not be used as first
			-- element of `a_arguments' (as it is the case with the
			-- `execve' C command.
			-- Also note that to start the tracing, `run' must be use.
		require
			Filename_Not_Empty: not a_filename.is_empty
			Filename_Exist_Readable: is_file_exists_and_readable(a_filename)
		do
			default_create
			create {ARRAYED_LIST[READABLE_STRING_GENERAL]} arguments.make_from_iterable (a_arguments)
			arguments.compare_objects
			filename := a_filename.twin
			create internal_filename.make ({UTF_CONVERTER}.string_32_to_utf_8_string_8 (filename.to_string_32))
			create {ARRAYED_LIST[C_STRING]}internal_arguments.make (arguments.count)
			across arguments as la_arguments loop
				internal_arguments.extend (
								create {C_STRING}.make (
										{UTF_CONVERTER}.string_32_to_utf_8_string_8 (la_arguments.item.to_string_32)
									)
							)
			end
		ensure
			Filename_Assign: filename ~ a_filename
			Arguments_Assign: arguments ~ arguments
		end

feature -- Access

	filename:READABLE_STRING_GENERAL
			-- The filename of the program to trace.

	arguments:LIST[READABLE_STRING_GENERAL]
			-- The arguments used in the program traced by `Current'.

feature -- Status setting

	run
			-- Execute the program `filename' with `arguments' in a new process
			-- and start the tracing.
		local
			l_index:INTEGER
			l_arguments:ARRAY[POINTER]
		do
			create l_arguments.make_filled (create {POINTER}, 0, internal_arguments.count + 1)
			l_arguments.put (internal_filename.item, 0)
			l_index := 1
			across internal_arguments as la_arguments loop
				l_arguments.put (la_arguments.item.item, l_index)
				l_index := l_index + 1
			end
			-- TODO: Manage error.
			pid := c_fork(internal_filename.item, $l_arguments)
		end

feature {NONE} -- Externals

	internal_arguments:LIST[C_STRING]
			-- Internal (C) representation of every `arguments'

	internal_filename:C_STRING
			-- Internal (C) reprensentation of `filename'

	c_fork(a_filename:POINTER; a_arguments:POINTER):INTEGER
			-- Create a fork and an exec of `a_filename' and return the
			-- new PID of the child.
		external
			"C inline use <sys/ptrace.h> , <sys/types.h>, <unistd.h>, <stdlib.h>"
		alias
			"[
				int l_pid =	fork();
				if (l_pid == 0){ // Children
					ptrace(PTRACE_TRACEME, 0, 0, 0);
					execv($a_filename, $a_arguments);
					exit(EXIT_FAILURE);
				}
				return l_pid;
			]"
		end

invariant
	Filename_Not_Empty: not filename.is_empty
	Internal_Filename_Valid: filename.as_string_32 ~ {UTF_CONVERTER}.utf_8_string_8_to_string_32 (internal_filename.string)
	Internal_Arguments_Valid: internal_arguments.count ~ arguments.count and
								across 1 |..| arguments.count as la_index all
									arguments.at (la_index.item).as_string_32 ~
										{UTF_CONVERTER}.utf_8_string_8_to_string_32 (
													internal_arguments.at (la_index.item).string
												)
								end
end
