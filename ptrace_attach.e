note
	description: "A {PTRACE} that attach to an already running process."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"

class
	PTRACE_ATTACH

inherit
	PTRACE

create
	make

feature {NONE} -- Initialisation

	make(a_pid:INTEGER)
			-- Initialisation of `Current' using `a_pid' as `pid'
			-- Note that to start the tracing, `attach' must be use.
		require
			PID_Valid: a_pid > 0
		do
			default_create
			pid := a_pid
		ensure
			PID_Assign: pid ~ a_pid
		end

feature -- Access

	attach
			-- Start to trace the process with ID `pid'
		do
			error_code := c_attach(pid)
		end

feature {NONE} -- Externals

	c_attach(a_pid:INTEGER):INTEGER
			-- C function to attach `Current' as the tracee of the process
			-- identified as `a_pid'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"ptrace(PTRACE_ATTACH, $a_pid, 0, 0)"
		end


invariant
	PID_Positive: pid > 0

end
