Eiffel ptrace
=============

An Eiffel library for using ptrace functionalities on Linux.

Compatibility
-------------

For the moment, only usable on Linux 32 bits and 64 bits.

Usage
-----

To be used, the `OS_PLATFORM` environment variable must be initialized like this
(on bash; must be adapted for other shell interpreter):

```bash
export OS_PLATFORM=`uname -s -m`
```
