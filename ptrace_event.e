note
	description: "A process wait event."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Thu, 03 Feb 2022 21:42:45 +0000"
	revision:    "0.1"

class
	PTRACE_EVENT

create {PTRACE}
	make

feature {NONE} -- initalisation

	make(a_item:INTEGER)
			-- Initialisation of `Current' using `a_item' as `item'
		do
			item := a_item
		ensure
			Item_Assign: a_item ~ item
		end

feature -- Access

	is_vfork:BOOLEAN
			-- Stop before return from vfork or clone with the CLONE_VFORK flag.
			-- When the tracee is continued after this stop, it will wait for child
			-- to exit/exec before continuing its execution
			-- (in other words, the usual behavior on vfork(2)).
		do
			Result := item.bit_and (c_ptrace_event_vfork.bit_shift_left (8)) /= 0
		end

	is_fork:BOOLEAN
			-- Stop before return from fork or clone with the
			-- {PTRACE_SIGNAL}.`is_child_stop' activated.
		do
			Result := item.bit_and (c_ptrace_event_fork.bit_shift_left (8)) /= 0
		end

	is_clone:BOOLEAN
			-- Stop before return from clone.
		do
			Result := item.bit_and (c_ptrace_event_clone.bit_shift_left (8)) /= 0
		end

	is_vfork_done:BOOLEAN
			-- Stop before return from vfork or clone with the CLONE_VFORK
			-- flag, but after the child unblocked this tracee by exiting or execing.
		do
			Result := item.bit_and (c_ptrace_event_vfork_done.bit_shift_left (8)) /= 0
		end

	is_exec:BOOLEAN
			-- Stop before return from execve.
		do
			Result := item.bit_and (c_ptrace_event_exec.bit_shift_left (8)) /= 0
		end

	is_exit:BOOLEAN
			-- Stop before exit (including death from exit_group), signal death, or exit
			-- caused by execve in a multithreaded process.
			-- The tracee is still alive; it needs to be continued or detached to finish exiting.
		do
			Result := item.bit_and (c_ptrace_event_exit.bit_shift_left (8)) /= 0
		end

	is_stop:BOOLEAN
			-- Stop induced by a {PTRACE}.`interrupt' command.
		do
			Result := item.bit_and (c_ptrace_event_stop.bit_shift_left (8)) /= 0
		end

	is_unused:BOOLEAN
			-- Is `Current' unused
		do
			Result := not (is_vfork or is_fork or is_clone or is_vfork_done or is_exec or is_exit or is_stop)
		end

feature {PTRACE_EVENT} -- Implementation

	item:INTEGER
			-- Internal reprensentation of `Current'

feature {NONE} -- Externals


	c_ptrace_event_vfork:INTEGER
			-- PTRACE_EVENT constant: Stop before return from vfork or clone
			-- with the CLONE_VFORK flag
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_VFORK"
		end

	c_ptrace_event_fork:INTEGER
			-- PTRACE_EVENT constant: Stop before return from fork or clone
			--  with the exit signal set to SIGCHLD
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_FORK"
		end

	c_ptrace_event_clone:INTEGER
			-- PTRACE_EVENT constant: Stop before return from clone
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_CLONE"
		end

	c_ptrace_event_vfork_done:INTEGER
			-- PTRACE_EVENT constant: Stop before return from vfork or clone
			-- with the CLONE_VFORK flag, but after the child unblocked this
			-- tracee by exiting or execing.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_VFORK_DONE"
		end

	c_ptrace_event_exec:INTEGER
			-- PTRACE_EVENT constant: Stop before return from execve
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_EXEC"
		end

	c_ptrace_event_exit:INTEGER
			-- PTRACE_EVENT constant: Stop before exit, signal death,
			-- or exit caused by execve in a multithreaded process.
			-- PTRACE_GETEVENTMSG returns the exit status.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_EXIT"
		end

	c_ptrace_event_stop:INTEGER
			-- PTRACE_EVENT constant: Stop induced by PTRACE_INTERRUPT command.
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_EVENT_STOP"
		end
end
