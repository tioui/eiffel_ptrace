note
	description: "Every registers usable in the system."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 11 May 2022 14:27:49 +0000"
	revision:    "0.1"

class
	PTRACE_REGISTER

inherit
	ANY
		redefine
			default_create
		end

create {PTRACE}
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			item := c_initialisation
		end

feature -- Access

	exists:BOOLEAN
		do
			Result := not item.is_default_pointer
		end



	ebx:INTEGER_32 assign set_ebx
			-- The value of the register ebx
		require
			Exists: exists
		do
			Result := c_get_ebx(item)
		end

	set_ebx(a_value:INTEGER_32)
			-- Assign the value of the register ebx with `a_value'
		require
			Exists: exists
		do
			c_set_ebx(item, a_value)
		end

	ecx:INTEGER_32 assign set_ecx
			-- The value of the register ecx
		require
			Exists: exists
		do
			Result := c_get_ecx(item)
		end

	set_ecx(a_value:INTEGER_32)
			-- Assign the value of the register ecx with `a_value'
		require
			Exists: exists
		do
			c_set_ecx(item, a_value)
		end

	edx:INTEGER_32 assign set_edx
			-- The value of the register edx
		require
			Exists: exists
		do
			Result := c_get_edx(item)
		end

	set_edx(a_value:INTEGER_32)
			-- Assign the value of the register edx with `a_value'
		require
			Exists: exists
		do
			c_set_edx(item, a_value)
		end

	esi:INTEGER_32 assign set_esi
			-- The value of the register esi
		require
			Exists: exists
		do
			Result := c_get_esi(item)
		end

	set_esi(a_value:INTEGER_32)
			-- Assign the value of the register esi with `a_value'
		require
			Exists: exists
		do
			c_set_esi(item, a_value)
		end

	edi:INTEGER_32 assign set_edi
			-- The value of the register edi
		require
			Exists: exists
		do
			Result := c_get_edi(item)
		end

	set_edi(a_value:INTEGER_32)
			-- Assign the value of the register edi with `a_value'
		require
			Exists: exists
		do
			c_set_edi(item, a_value)
		end

	ebp:INTEGER_32 assign set_ebp
			-- The value of the register ebp
		require
			Exists: exists
		do
			Result := c_get_ebp(item)
		end

	set_ebp(a_value:INTEGER_32)
			-- Assign the value of the register ebp with `a_value'
		require
			Exists: exists
		do
			c_set_ebp(item, a_value)
		end

	eax:INTEGER_32 assign set_eax
			-- The value of the register eax
		require
			Exists: exists
		do
			Result := c_get_eax(item)
		end

	set_eax(a_value:INTEGER_32)
			-- Assign the value of the register eax with `a_value'
		require
			Exists: exists
		do
			c_set_eax(item, a_value)
		end

	xds:INTEGER_32 assign set_xds
			-- The value of the register xds
		require
			Exists: exists
		do
			Result := c_get_xds(item)
		end

	set_xds(a_value:INTEGER_32)
			-- Assign the value of the register xds with `a_value'
		require
			Exists: exists
		do
			c_set_xds(item, a_value)
		end

	xes:INTEGER_32 assign set_xes
			-- The value of the register xes
		require
			Exists: exists
		do
			Result := c_get_xes(item)
		end

	set_xes(a_value:INTEGER_32)
			-- Assign the value of the register xes with `a_value'
		require
			Exists: exists
		do
			c_set_xes(item, a_value)
		end

	xfs:INTEGER_32 assign set_xfs
			-- The value of the register xfs
		require
			Exists: exists
		do
			Result := c_get_xfs(item)
		end

	set_xfs(a_value:INTEGER_32)
			-- Assign the value of the register xfs with `a_value'
		require
			Exists: exists
		do
			c_set_xfs(item, a_value)
		end

	xgs:INTEGER_32 assign set_xgs
			-- The value of the register xgs
		require
			Exists: exists
		do
			Result := c_get_xgs(item)
		end

	set_xgs(a_value:INTEGER_32)
			-- Assign the value of the register xgs with `a_value'
		require
			Exists: exists
		do
			c_set_xgs(item, a_value)
		end

	orig_eax:INTEGER_32 assign set_orig_eax
			-- The value of the register orig_eax
		require
			Exists: exists
		do
			Result := c_get_orig_eax(item)
		end

	set_orig_eax(a_value:INTEGER_32)
			-- Assign the value of the register orig_eax with `a_value'
		require
			Exists: exists
		do
			c_set_orig_eax(item, a_value)
		end

	eip:INTEGER_32 assign set_eip
			-- The value of the register eip
		require
			Exists: exists
		do
			Result := c_get_eip(item)
		end

	set_eip(a_value:INTEGER_32)
			-- Assign the value of the register eip with `a_value'
		require
			Exists: exists
		do
			c_set_eip(item, a_value)
		end

	xcs:INTEGER_32 assign set_xcs
			-- The value of the register xcs
		require
			Exists: exists
		do
			Result := c_get_xcs(item)
		end

	set_xcs(a_value:INTEGER_32)
			-- Assign the value of the register xcs with `a_value'
		require
			Exists: exists
		do
			c_set_xcs(item, a_value)
		end

	eflags:INTEGER_32 assign set_eflags
			-- The value of the register eflags
		require
			Exists: exists
		do
			Result := c_get_eflags(item)
		end

	set_eflags(a_value:INTEGER_32)
			-- Assign the value of the register eflags with `a_value'
		require
			Exists: exists
		do
			c_set_eflags(item, a_value)
		end

	esp:INTEGER_32 assign set_esp
			-- The value of the register esp
		require
			Exists: exists
		do
			Result := c_get_esp(item)
		end

	set_esp(a_value:INTEGER_32)
			-- Assign the value of the register esp with `a_value'
		require
			Exists: exists
		do
			c_set_esp(item, a_value)
		end

	xss:INTEGER_32 assign set_xss
			-- The value of the register xss
		require
			Exists: exists
		do
			Result := c_get_xss(item)
		end

	set_xss(a_value:INTEGER_32)
			-- Assign the value of the register xss with `a_value'
		require
			Exists: exists
		do
			c_set_xss(item, a_value)
		end

feature {NONE} -- C Externals


	c_get_ebx(a_item:POINTER):INTEGER_32
			-- Get the ebx register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ebx"
		end

	c_set_ebx(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the ebx register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ebx = $a_value"
		end

	c_get_ecx(a_item:POINTER):INTEGER_32
			-- Get the ecx register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ecx"
		end

	c_set_ecx(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the ecx register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ecx = $a_value"
		end

	c_get_edx(a_item:POINTER):INTEGER_32
			-- Get the edx register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->edx"
		end

	c_set_edx(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the edx register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->edx = $a_value"
		end

	c_get_esi(a_item:POINTER):INTEGER_32
			-- Get the esi register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->esi"
		end

	c_set_esi(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the esi register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->esi = $a_value"
		end

	c_get_edi(a_item:POINTER):INTEGER_32
			-- Get the edi register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->edi"
		end

	c_set_edi(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the edi register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->edi = $a_value"
		end

	c_get_ebp(a_item:POINTER):INTEGER_32
			-- Get the ebp register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ebp"
		end

	c_set_ebp(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the ebp register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ebp = $a_value"
		end

	c_get_eax(a_item:POINTER):INTEGER_32
			-- Get the eax register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eax"
		end

	c_set_eax(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the eax register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eax = $a_value"
		end

	c_get_xds(a_item:POINTER):INTEGER_32
			-- Get the xds register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xds"
		end

	c_set_xds(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the xds register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xds = $a_value"
		end

	c_get_xes(a_item:POINTER):INTEGER_32
			-- Get the xes register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xes"
		end

	c_set_xes(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the xes register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xes = $a_value"
		end

	c_get_xfs(a_item:POINTER):INTEGER_32
			-- Get the xfs register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xfs"
		end

	c_set_xfs(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the xfs register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xfs = $a_value"
		end

	c_get_xgs(a_item:POINTER):INTEGER_32
			-- Get the xgs register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xgs"
		end

	c_set_xgs(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the xgs register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xgs = $a_value"
		end

	c_get_orig_eax(a_item:POINTER):INTEGER_32
			-- Get the orig_eax register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->orig_eax"
		end

	c_set_orig_eax(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the orig_eax register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->orig_eax = $a_value"
		end

	c_get_eip(a_item:POINTER):INTEGER_32
			-- Get the eip register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eip"
		end

	c_set_eip(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the eip register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eip = $a_value"
		end

	c_get_xcs(a_item:POINTER):INTEGER_32
			-- Get the xcs register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xcs"
		end

	c_set_xcs(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the xcs register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xcs = $a_value"
		end

	c_get_eflags(a_item:POINTER):INTEGER_32
			-- Get the eflags register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eflags"
		end

	c_set_eflags(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the eflags register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eflags = $a_value"
		end

	c_get_esp(a_item:POINTER):INTEGER_32
			-- Get the esp register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->esp"
		end

	c_set_esp(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the esp register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->esp = $a_value"
		end

	c_get_xss(a_item:POINTER):INTEGER_32
			-- Get the xss register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xss"
		end

	c_set_xss(a_item:POINTER; a_value:INTEGER_32)
			-- Assign the xss register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->xss = $a_value"
		end


	c_initialisation:POINTER
			-- Initialise a 'user_regs_struct' structure
		external
			"C inline use <sys/user.h>"
		alias
			"calloc(1, sizeof(struct user_regs_struct))"
		end


feature {PTRACE} -- Implementation

	item:POINTER
			-- The internal pointeur of `Current'

end
