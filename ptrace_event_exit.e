note
	description: "A {PTRACE_EVENT} for an exiting process."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Thu, 03 Feb 2022 21:42:45 +0000"
	revision:    "0.1"

class
	PTRACE_EVENT_EXIT

inherit
	PTRACE_EVENT

create {PTRACE}
	make_from_event

feature {NONE} -- Initialisation

	make_from_event(a_event:PTRACE_EVENT; a_exit_status:INTEGER)
			-- Initialisation of `Current' using `a_event' as an exit event
			-- and using `a_exit_status' as `exit_status'.
		require
			Is_Exit_Event: a_event.is_exit
		do
			item := a_event.item
			exit_status := a_exit_status
		ensure
			Is_Item_Assign: item ~ a_event.item
			Is_Exit_Status_Assign: exit_status ~ a_exit_status
		end

feature -- Access

	exit_status:INTEGER
			-- The exit status of the child process

invariant
	Is_Exit_Event: is_exit
end
