note
	description: "A process signal."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Thu, 03 Feb 2022 21:42:45 +0000"
	revision:    "0.1"

class
	PTRACE_SIGNAL_READABLE

inherit
	ANY
		redefine
			is_equal
		end

create {PTRACE}
	make_from_status

create {PTRACE, PTRACE_SIGNAL_INFORMATION}
	make

create
	make_from_other

feature {PTRACE} -- Class methods

	is_signal_status(a_status:INTEGER):BOOLEAN
			-- Return `True' if the `a_status' represent a stop by signal
		do
			Result := c_wifstopped(a_status)
		ensure
			class
		end

feature {NONE} -- Initialisation

	make_from_other(a_other:PTRACE_SIGNAL_READABLE)
			-- Initialisation of `Current' copied from `a_other'
		do
			item := a_other.item
		ensure
			Is_Item_Valid: item ~ a_other.item
		end

	make_from_status(a_status:INTEGER)
			-- Initialisation of `Current' using `a_status' to initialize `item'
		require
			Is_Signal_Status: is_signal_status(a_status)
		do
			item := c_wstopsig(a_status)
		end

	make(a_item:INTEGER)
			-- Initialisation of `Current' using `a_item' as `item'
		do
			item  := a_item
		ensure
			Is_Assign: item ~ a_item
		end

feature -- Access

	is_abort:BOOLEAN
			-- `Current' is an abort signal
		do
			Result := item ~ c_sigabrt
		end

	is_timer:BOOLEAN
			-- `Current' is a timer signal
		do
			Result := item ~ c_sigalrm
		end

	is_bus_error:BOOLEAN
			-- `Current' is a bus error signal
		do
			Result := item ~ c_sigbus
		end

	is_child_stop:BOOLEAN
			-- `Current' is a Child stopped or terminated signal
		do
			Result := item ~ c_sigchld
		end

	is_continue:BOOLEAN
			-- `Current' is a continue if stopped signal
		do
			Result := item ~ c_sigcont
		end

	is_floating_point_exception:BOOLEAN
			-- `Current' is a floating point exception signal
		do
			Result := item ~ c_sigfpe
		end

	is_hangup:BOOLEAN
			-- `Current' is a hangup (or death of controlling process) signal
		do
			Result := item ~ c_sighup
		end

	is_illegal_instruction:BOOLEAN
			-- `Current' is an illegal instruction signal
		do
			Result := item ~ c_sigill
		end

	is_power_failure:BOOLEAN
			-- `Current' is an illegal instruction signal (System V)
		do
			Result := item ~ c_sigpwr
		end

	is_keyboard_interrupt:BOOLEAN
			-- `Current' is a keyboard interrupt signal
		do
			Result := item ~ c_sigint
		end

	is_io:BOOLEAN
			-- `Current' is I/O now possible signal (4.2BSD)
		do
			Result := item ~ c_sigio
		end

	is_iot:BOOLEAN
			-- `Current' is an IOT trap
		do
			Result := item ~ c_sigiot
		end

	is_kill:BOOLEAN
			-- `Current' is a kill signal
		do
			Result := item ~ c_sigkill
		end

	is_broken_pipe:BOOLEAN
			-- `Current' is a broken pipe signal
		do
			Result := item ~ c_sigpipe
		end

	is_pollable:BOOLEAN
			-- `Current' is a pollable event (System V)
		do
			Result := item ~ c_sigpoll
		end

	is_profiling_timer_expired:BOOLEAN
			-- `Current' is a profiling timer expired event
		do
			Result := item ~ c_sigprof
		end

	is_quit:BOOLEAN
			-- `Current' is a quit from keyboard signal
		do
			Result := item ~ c_sigquit
		end

	is_segmentation_fault:BOOLEAN
			-- `Current' is an invalid memory reference signal
		do
			Result := item ~ c_sigsegv
		end

	is_stack_fault:BOOLEAN
			-- `Current' is an stack fault on coprocessor signal (unused)
		do
			Result := item ~ c_sigstkflt
		end

	is_stop:BOOLEAN
			-- `Current' is a stop process signal
		do
			Result := item ~ c_sigstop
		end

	is_terminal_stop:BOOLEAN
			-- `Current' is a stop typed at terminals signal
		do
			Result := item ~ c_sigtstp
		end

	is_bad_system_call:BOOLEAN
			-- `Current' is a bad system call signal (SVr4)
		do
			Result := item ~ c_sigsys
		end

	is_termination:BOOLEAN
			-- `Current' is a termination signal
		do
			Result := item ~ c_sigterm
		end

	is_trace:BOOLEAN
			-- `Current' is a trace/breakpoint trap signal
		do
			Result := item ~ c_sigtrap
		end

	is_terminal_input_baground:BOOLEAN
			-- `Current' is a terminal input for background process signal
		do
			Result := item ~ c_sigttin
		end

	is_terminal_output_baground:BOOLEAN
			-- `Current' is a terminal output for background process signal
		do
			Result := item ~ c_sigttou
		end

	is_urgent_condition_on_socket:BOOLEAN
			-- `Current' is an urgent condition on socket signal (4.2BSD)
		do
			Result := item ~ c_sigttou
		end

	is_user1:BOOLEAN
			-- `Current' is a first user-defined signal
		do
			Result := item ~ c_sigusr1
		end

	is_user2:BOOLEAN
			-- `Current' is a second user-defined signall
		do
			Result := item ~ c_sigusr2
		end

	is_virtual_alarm_clock:BOOLEAN
			-- `Current' is a virtual alarm clock signal (4.2BSD)
		do
			Result := item ~ c_sigvtalrm
		end

	is_cpu_time_limit_exceeded:BOOLEAN
			-- `Current' is a CPU time limit exceeded signal (4.2BSD)
		do
			Result := item ~ c_sigxcpu
		end

	is_file_size_limit_exceeded:BOOLEAN
			-- `Current' is a file size limit exceeded signal (4.2BSD)
		do
			Result := item ~ c_sigxfsz
		end

	is_window_resized:BOOLEAN
			-- `Current' is a Window resize signal (4.3BSD, Sun)
		do
			Result := item ~ c_sigxfsz
		end

feature -- Comparison

	is_equal (a_other: like Current): BOOLEAN
			-- Is `a_other' attached to an object considered
			-- equal to current object?
		do
			Result := a_other.item ~ item
		end

feature {PTRACE_SIGNAL_READABLE, PTRACE_SIGNAL_INFORMATION} -- Implementation

	item:INTEGER
			-- Internal representation of `Current'

feature {NONE} -- External

	c_wifstopped(a_status:INTEGER):BOOLEAN
			-- Returns true if the child process was stopped by delivery
			-- of a signal
		external
			"C inline use <sys/wait.h>"
		alias
			"WIFSTOPPED($a_status)"
		end

	c_wstopsig(a_status:INTEGER):INTEGER
			-- Returns the number of the signal which caused the child to
			-- stop.
		external
			"C inline use <sys/wait.h>"
		alias
			"WSTOPSIG($a_status)"
		end

	c_sigabrt:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGABRT"
		end

	c_sigalrm:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGALRM"
		end

	c_sigbus:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGBUS"
		end

	c_sigchld:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGCHLD"
		end

	c_sigcont:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGCONT"
		end

	c_sigfpe:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGFPE"
		end

	c_sighup:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGHUP"
		end

	c_sigill:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGILL"
		end

	c_sigint:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGINT"
		end

	c_sigio:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGIO"
		end

	c_sigiot:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGIOT"
		end

	c_sigkill:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGKILL"
		end

	c_sigpipe:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGPIPE"
		end

	c_sigpoll:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGPOLL"
		end

	c_sigprof:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGPROF"
		end

	c_sigpwr:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGPWR"
		end

	c_sigquit:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGQUIT"
		end

	c_sigsegv:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGSEGV"
		end

	c_sigstkflt:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGSTKFLT"
		end

	c_sigstop:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGSTOP"
		end

	c_sigtstp:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGTSTP"
		end

	c_sigsys:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGSYS"
		end

	c_sigterm:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGTERM"
		end

	c_sigtrap:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGTRAP"
		end

	c_sigttin:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGTTIN"
		end

	c_sigttou:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGTTOU"
		end

	c_sigurg:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGURG"
		end

	c_sigusr1:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGUSR1"
		end

	c_sigusr2:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGUSR2"
		end

	c_sigvtalrm:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGVTALRM"
		end

	c_sigxcpu:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGXCPU"
		end

	c_sigxfsz:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGXFSZ"
		end

	c_sigwinch:INTEGER
		external
			"C inline use <signal.h>"
		alias
			"SIGWINCH"
		end


end
