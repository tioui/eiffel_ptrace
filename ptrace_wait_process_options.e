note
	description: "Options used on the {PTRACE}.`wait_process' method."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Thu, 03 Feb 2022 21:42:45 +0000"
	revision:    "0.1"

class
	PTRACE_WAIT_PROCESS_OPTIONS

inherit
	ANY
		redefine
			default_create
		end

create
	default_create,
	make_from_other

create {PTRACE_WAIT_PROCESS_OPTIONS, PTRACE}
	make_from_item

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			make_from_item(0)
		end

	make_from_other(a_other:PTRACE_WAIT_PROCESS_OPTIONS)
			-- Initialisation of `Current' using the values in `a_other'
		do
			make_from_item(a_other.item)
		end

	make_from_item(a_item:INTEGER)
			-- Initialisation of `Current' using `a_item' as `item'
		do
			item := a_item
		end

feature {PTRACE_WAIT_PROCESS_OPTIONS, PTRACE} -- Implementation

	item:INTEGER
			-- Interal representation of `Current'

end
