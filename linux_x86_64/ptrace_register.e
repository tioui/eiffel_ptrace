note
	description: "Every registers usable in the system."
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 11 May 2022 14:27:49 +0000"
	revision:    "0.1"

class
	PTRACE_REGISTER

inherit
	ANY
		redefine
			default_create
		end

create {PTRACE}
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			item := c_initialisation
		end


feature -- Access

	exists:BOOLEAN
		do
			Result := not item.is_default_pointer
		end

	r15:NATURAL_64 assign set_r15
			-- The value of the register r15
		require
			Exists: exists
		do
			Result := c_get_r15(item)
		end

	set_r15(a_value:NATURAL_64)
			-- Assign the value of the register r15 with `a_value'
		require
			Exists: exists
		do
			c_set_r15(item, a_value)
		end

	r14:NATURAL_64 assign set_r14
			-- The value of the register r14
		require
			Exists: exists
		do
			Result := c_get_r14(item)
		end

	set_r14(a_value:NATURAL_64)
			-- Assign the value of the register r14 with `a_value''
		require
			Exists:exists
		do
			c_set_r14(item, a_value)
		end

	r13:NATURAL_64 assign set_r13
			-- The value of the register r13
		require
			Exists: exists
		do
			Result := c_get_r13(item)
		end

	set_r13(a_value:NATURAL_64)
			-- Assign the value of the register r13 with `a_value'
		require
			Exists:exists
		do
			c_set_r13(item, a_value)
		end

	r12:NATURAL_64 assign set_r12
			-- The value of the register r12
		require
			Exists: exists
		do
			Result := c_get_r12(item)
		end

	set_r12(a_value:NATURAL_64)
			-- Assign the value of the register r12 with `a_value'
		require
			Exists:exists
		do
			c_set_r12(item, a_value)
		end

	rbp:NATURAL_64 assign set_rbp
			-- The value of the register rbp
		require
			Exists: exists
		do
			Result := c_get_rbp(item)
		end

	set_rbp(a_value:NATURAL_64)
			-- Assign the value of the register rbp with `a_value'
		require
			Exists:exists
		do
			c_set_rbp(item, a_value)
		end

	rbx:NATURAL_64 assign set_rbx
			-- The value of the register rbx
		require
			Exists: exists
		do
			Result := c_get_rbx(item)
		end

	set_rbx(a_value:NATURAL_64)
			-- Assign the value of the register rbx with `a_value'
		require
			Exists:exists
		do
			c_set_rbx(item, a_value)
		end

	r11:NATURAL_64 assign set_r11
			-- The value of the register r11
		require
			Exists: exists
		do
			Result := c_get_r11(item)
		end

	set_r11(a_value:NATURAL_64)
			-- Assign the value of the register r11 with `a_value'
		require
			Exists:exists
		do
			c_set_r11(item, a_value)
		end

	r10:NATURAL_64 assign set_r10
			-- The value of the register r10
		require
			Exists: exists
		do
			Result := c_get_r10(item)
		end

	set_r10(a_value:NATURAL_64)
			-- Assign the value of the register r10 with `a_value'
		require
			Exists:exists
		do
			c_set_r10(item, a_value)
		end

	r9:NATURAL_64 assign set_r9
			-- The value of the register r9
		require
			Exists: exists
		do
			Result := c_get_r9(item)
		end

	set_r9(a_value:NATURAL_64)
			-- Assign the value of the register r9 with `a_value'
		require
			Exists:exists
		do
			c_set_r9(item, a_value)
		end

	r8:NATURAL_64 assign set_r8
			-- The value of the register r8
		require
			Exists: exists
		do
			Result := c_get_r8(item)
		end

	set_r8(a_value:NATURAL_64)
			-- Assign the value of the register r8 with `a_value'
		require
			Exists:exists
		do
			c_set_r8(item, a_value)
		end

	rax:NATURAL_64 assign set_rax
			-- The value of the register rax
		require
			Exists: exists
		do
			Result := c_get_rax(item)
		end

	set_rax(a_value:NATURAL_64)
			-- Assign the value of the register rax with `a_value'
		require
			Exists:exists
		do
			c_set_rax(item, a_value)
		end

	rcx:NATURAL_64 assign set_rcx
			-- The value of the register rcx
		require
			Exists: exists
		do
			Result := c_get_rcx(item)
		end

	set_rcx(a_value:NATURAL_64)
			-- Assign the value of the register rcx with `a_value'
		require
			Exists:exists
		do
			c_set_rcx(item, a_value)
		end

	rdx:NATURAL_64 assign set_rdx
			-- The value of the register rdx
		require
			Exists: exists
		do
			Result := c_get_rdx(item)
		end

	set_rdx(a_value:NATURAL_64)
			-- Assign the value of the register rdx with `a_value'
		require
			Exists:exists
		do
			c_set_rdx(item, a_value)
		end

	rsi:NATURAL_64 assign set_rsi
			-- The value of the register rsi
		require
			Exists: exists
		do
			Result := c_get_rsi(item)
		end

	set_rsi(a_value:NATURAL_64)
			-- Assign the value of the register rsi with `a_value'
		require
			Exists:exists
		do
			c_set_rsi(item, a_value)
		end

	rdi:NATURAL_64 assign set_rdi
			-- The value of the register rdi
		require
			Exists: exists
		do
			Result := c_get_rdi(item)
		end

	set_rdi(a_value:NATURAL_64)
			-- Assign the value of the register rdi with `a_value'
		require
			Exists:exists
		do
			c_set_rdi(item, a_value)
		end

	orig_rax:NATURAL_64 assign set_orig_rax
			-- The value of the register orig_rax
		require
			Exists: exists
		do
			Result := c_get_orig_rax(item)
		end

	set_orig_rax(a_value:NATURAL_64)
			-- Assign the value of the register orig_rax with `a_value'
		require
			Exists:exists
		do
			c_set_orig_rax(item, a_value)
		end

	rip:NATURAL_64 assign set_rip
			-- The value of the register rip
		require
			Exists: exists
		do
			Result := c_get_rip(item)
		end

	set_rip(a_value:NATURAL_64)
			-- Assign the value of the register rip with `a_value'
		require
			Exists:exists
		do
			c_set_rip(item, a_value)
		end

	cs:NATURAL_64 assign set_cs
			-- The value of the register cs
		require
			Exists: exists
		do
			Result := c_get_cs(item)
		end

	set_cs(a_value:NATURAL_64)
			-- Assign the value of the register cs with `a_value'
		require
			Exists:exists
		do
			c_set_cs(item, a_value)
		end

	eflags:NATURAL_64 assign set_eflags
			-- The value of the register eflags
		require
			Exists: exists
		do
			Result := c_get_eflags(item)
		end

	set_eflags(a_value:NATURAL_64)
			-- Assign the value of the register eflags with `a_value'
		require
			Exists:exists
		do
			c_set_eflags(item, a_value)
		end

	rsp:NATURAL_64 assign set_rsp
			-- The value of the register rsp
		require
			Exists: exists
		do
			Result := c_get_rsp(item)
		end

	set_rsp(a_value:NATURAL_64)
			-- Assign the value of the register rsp with `a_value'
		require
			Exists:exists
		do
			c_set_rsp(item, a_value)
		end

	ss:NATURAL_64 assign set_ss
			-- The value of the register ss
		require
			Exists: exists
		do
			Result := c_get_ss(item)
		end

	set_ss(a_value:NATURAL_64)
			-- Assign the value of the register ss with `a_value'
		require
			Exists:exists
		do
			c_set_ss(item, a_value)
		end

	fs_base:NATURAL_64 assign set_fs_base
			-- The value of the register fs_base
		require
			Exists: exists
		do
			Result := c_get_fs_base(item)
		end

	set_fs_base(a_value:NATURAL_64)
			-- Assign the value of the register fs_base with `a_value'
		require
			Exists:exists
		do
			c_set_fs_base(item, a_value)
		end

	gs_base:NATURAL_64 assign set_gs_base
			-- The value of the register gs_base
		require
			Exists: exists
		do
			Result := c_get_gs_base(item)
		end

	set_gs_base(a_value:NATURAL_64)
			-- Assign the value of the register gs_base with `a_value'
		require
			Exists:exists
		do
			c_set_gs_base(item, a_value)
		end

	ds:NATURAL_64 assign set_ds
			-- The value of the register ds
		require
			Exists: exists
		do
			Result := c_get_ds(item)
		end

	set_ds(a_value:NATURAL_64)
			-- Assign the value of the register ds with `a_value'
		require
			Exists:exists
		do
			c_set_ds(item, a_value)
		end

	es:NATURAL_64 assign set_es
			-- The value of the register es
		require
			Exists: exists
		do
			Result := c_get_es(item)
		end

	set_es(a_value:NATURAL_64)
			-- Assign the value of the register es with `a_value'
		require
			Exists:exists
		do
			c_set_es(item, a_value)
		end

	fs:NATURAL_64 assign set_fs
			-- The value of the register fs
		require
			Exists: exists
		do
			Result := c_get_fs(item)
		end

	set_fs(a_value:NATURAL_64)
			-- Assign the value of the register fs with `a_value'
		require
			Exists:exists
		do
			c_set_fs(item, a_value)
		end

	gs:NATURAL_64 assign set_gs
			-- The value of the register gs
		require
			Exists: exists
		do
			Result := c_get_gs(item)
		end

	set_gs(a_value:NATURAL_64)
			-- Assign the value of the register gs with `a_value'
		require
			Exists:exists
		do
			c_set_gs(item, a_value)
		end


feature {NONE} -- C external

--I	c_get_b(a_item:POINTER):NATURAL_64		-- Get the b register of `a_item'�kbexternal	"C inline use <sys/user.h>"�kbalias	"((struct user_regs_struct*)$a_item)->b"�kbend�kbc_set_b(a_item:POINTER; a_value:NATURAL_64)		-- Assign the sp�kb�kbb register of `a_item' with `a_value'�kbExternal�kb�kb�kb�kb�kb�kb�kb�kb�kb�kb�kbexternal	"C inline use <sys/user.h>"�kbalias	"�PS((struct user_regs_struct*)$a_item)->[201~b = $a_value"�kbend��a

	c_get_r15(a_item:POINTER):NATURAL_64
			-- Get the r15 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r15"
		end

	c_set_r15(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r15 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r15 = $a_value"
		end

	c_get_r14(a_item:POINTER):NATURAL_64
			-- Get the r14 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r14"
		end

	c_set_r14(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r14 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r14 = $a_value"
		end

	c_get_r13(a_item:POINTER):NATURAL_64
			-- Get the r13 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r13"
		end

	c_set_r13(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r13 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r13 = $a_value"
		end

	c_get_r12(a_item:POINTER):NATURAL_64
			-- Get the r12 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r12"
		end

	c_set_r12(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r12 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r12 = $a_value"
		end

	c_get_rbp(a_item:POINTER):NATURAL_64
			-- Get the rbp register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rbp"
		end

	c_set_rbp(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rbp register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rbp = $a_value"
		end

	c_get_rbx(a_item:POINTER):NATURAL_64
			-- Get the rbx register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rbx"
		end

	c_set_rbx(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rbx register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rbx = $a_value"
		end

	c_get_r11(a_item:POINTER):NATURAL_64
			-- Get the r11 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r11"
		end

	c_set_r11(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r11 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r11 = $a_value"
		end

	c_get_r10(a_item:POINTER):NATURAL_64
			-- Get the r10 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r10"
		end

	c_set_r10(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r10 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r10 = $a_value"
		end

	c_get_r9(a_item:POINTER):NATURAL_64
			-- Get the r9 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r9"
		end

	c_set_r9(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r9 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r9 = $a_value"
		end

	c_get_r8(a_item:POINTER):NATURAL_64
			-- Get the r8 register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r8"
		end

	c_set_r8(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the r8 register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->r8 = $a_value"
		end

	c_get_rax(a_item:POINTER):NATURAL_64
			-- Get the rax register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rax"
		end

	c_set_rax(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rax register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rax = $a_value"
		end

	c_get_rcx(a_item:POINTER):NATURAL_64
			-- Get the rcx register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rcx"
		end

	c_set_rcx(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rcx register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rcx = $a_value"
		end

	c_get_rdx(a_item:POINTER):NATURAL_64
			-- Get the rdx register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rdx"
		end

	c_set_rdx(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rdx register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rdx = $a_value"
		end

	c_get_rsi(a_item:POINTER):NATURAL_64
			-- Get the rsi register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rsi"
		end

	c_set_rsi(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rsi register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rsi = $a_value"
		end

	c_get_rdi(a_item:POINTER):NATURAL_64
			-- Get the rdi register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rdi"
		end

	c_set_rdi(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rdi register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rdi = $a_value"
		end

	c_get_orig_rax(a_item:POINTER):NATURAL_64
			-- Get the orig_rax register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->orig_rax"
		end

	c_set_orig_rax(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the orig_rax register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->orig_rax = $a_value"
		end

	c_get_rip(a_item:POINTER):NATURAL_64
			-- Get the rip register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rip"
		end

	c_set_rip(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rip register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rip = $a_value"
		end

	c_get_cs(a_item:POINTER):NATURAL_64
			-- Get the cs register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->cs"
		end

	c_set_cs(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the cs register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->cs = $a_value"
		end

	c_get_eflags(a_item:POINTER):NATURAL_64
			-- Get the eflags register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eflags"
		end

	c_set_eflags(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the eflags register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->eflags = $a_value"
		end

	c_get_rsp(a_item:POINTER):NATURAL_64
			-- Get the rsp register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rsp"
		end

	c_set_rsp(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the rsp register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->rsp = $a_value"
		end

	c_get_ss(a_item:POINTER):NATURAL_64
			-- Get the ss register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ss"
		end

	c_set_ss(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the ss register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ss = $a_value"
		end

	c_get_fs_base(a_item:POINTER):NATURAL_64
			-- Get the fs_base register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->fs_base"
		end

	c_set_fs_base(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the fs_base register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->fs_base = $a_value"
		end

	c_get_gs_base(a_item:POINTER):NATURAL_64
			-- Get the gs_base register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->gs_base"
		end

	c_set_gs_base(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the gs_base register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->gs_base = $a_value"
		end

	c_get_ds(a_item:POINTER):NATURAL_64
			-- Get the ds register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ds"
		end

	c_set_ds(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the ds register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->ds = $a_value"
		end

	c_get_es(a_item:POINTER):NATURAL_64
			-- Get the es register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->es"
		end

	c_set_es(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the es register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->es = $a_value"
		end

	c_get_fs(a_item:POINTER):NATURAL_64
			-- Get the fs register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->fs"
		end

	c_set_fs(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the fs register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->fs = $a_value"
		end

	c_get_gs(a_item:POINTER):NATURAL_64
			-- Get the gs register of `a_item'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->gs"
		end

	c_set_gs(a_item:POINTER; a_value:NATURAL_64)
			-- Assign the gs register of `a_item' with `a_value'
		external
			"C inline use <sys/user.h>"
		alias
			"((struct user_regs_struct*)$a_item)->gs = $a_value"
		end

	c_initialisation:POINTER
			-- Initialise a 'user_regs_struct' structure
		external
			"C inline use <sys/user.h>"
		alias
			"calloc(1, sizeof(struct user_regs_struct))"
		end


feature {PTRACE} -- Implementation

	item:POINTER
			-- The internal pointeur of `Current'

end
