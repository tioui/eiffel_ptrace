note
	description: "gedb application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l_trace1:PTRACE_ATTACH
			l_trace2:PTRACE_FILE
		do
			--| Add your code here
			print ("Hello Eiffel World!%N")
		end

end
