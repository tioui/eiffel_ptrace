note
	description: "Informations about a dignal catch by a {PTRACE}."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"
	ToDo:        "TODO: fields"

class
	PTRACE_SIGNAL_INFORMATION

inherit
	DISPOSABLE
		redefine
			default_create
		end

create
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			item := {POINTER}.memory_calloc (1, c_sizeof_siginfo_t)
		end

feature -- Access

	exists:BOOLEAN
			-- `Current' have an actual value.
		do
			Result := not item.is_default_pointer
		end

	signal: PTRACE_SIGNAL_READABLE assign set_signal
			-- The signal that `Current' specify
		require
			Exists: exists
		do
			create Result.make (c_get_si_signo(item))
		end

	error_number: INTEGER assign set_error_number
			-- The error number of `Current'
		require
			Exists: exists
		do
			Result := c_get_si_errno(item)
		end

	code: INTEGER assign set_code
			-- The code of `Current'
			-- Todo: Class for code.
		require
			Exists: exists
		do
			Result := c_get_si_code(item)
		end

	process_id:INTEGER assign set_process_id
			-- The sending process ID of `Current'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			Result := c_get_si_pid(item)
		end

	user_id:INTEGER assign set_user_id
			-- The real user ID of sending process of `Current'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			Result := c_get_si_uid(item)
		end

	user_time_consumed:INTEGER assign set_user_time_consumed
			-- The user time used by the child process of `Current'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			Result := c_get_si_utime(item)
		end

	system_time_consumed:INTEGER assign set_system_time_consumed
			-- The system time used by the child process of `Current'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			Result := c_get_si_stime(item)
		end

	value:INTEGER assign set_value
			-- The signal value of `Current'
		require
			Exists: exists
		do
			Result := c_get_si_value_int(item)
		end

	value_pointer:POINTER assign set_value_pointer
			-- The signal `value' of `Current' as a {POINTER}.
		require
			Exists: exists
		do
			Result := c_get_si_value_pointer(item)
		end

	posix_1b_value:INTEGER assign set_posix_1b_value
			-- The signal POSIX 1b value of `Current'
		require
			Exists: exists
		do
			Result := c_get_si_int(item)
		end

	posix_1b_value_pointer:POINTER assign set_posix_1b_value_pointer
			-- The signal POSIX 1b `value' of `Current' as a {POINTER}.
		require
			Exists: exists
		do
			Result := c_get_si_ptr(item)
		end

	overrun_count:INTEGER assign set_overrun_count
			-- The timer overrun count for POSIX.1b timers of `Current'
		require
			Exists: exists
		do
			Result := c_get_si_overrun(item)
		end

	timer_id:INTEGER assign set_timer_id
			-- The timer ID for POSIX.1b timers of `Current'
		require
			Exists: exists
		do
			Result := c_get_si_timerid(item)
		end

	memory_fault_address:POINTER assign set_memory_fault_address
			-- The memory location which caused fault
		require
			Exists: exists
			Is_Signal_Valid: signal.is_kill or signal.is_floating_point_exception or
							signal.is_segmentation_fault or signal.is_bus_error or
							signal.is_trace
		do
			Result := c_get_si_addr(item)
		end

	band_event:INTEGER assign set_band_event
			-- The band event of `Current'
			-- (only used in SIGIO/SIGPOLL)
		require
			Exists: exists
			Is_Signal_Valid: signal.is_io or signal.is_pollable
		do
			Result := c_get_si_band(item)
		end

	file_descriptor:INTEGER assign set_file_descriptor
			-- The descriptor of a file openned by the process of `Current'
			-- (only used in SIGIO/SIGPOLL)
		require
			Exists: exists
			Is_Signal_Valid: signal.is_io or signal.is_pollable
		do
			Result := c_get_si_fd(item)
		end

	memory_fault_least_significant_bits:INTEGER_16 assign set_memory_fault_least_significant_bits
			-- The least significant bit of a `memory_fault_address'
			-- (since Linux kernel 2.6.32)
		require
			Exists: exists
			Is_Signal_Valid: signal.is_kill or signal.is_floating_point_exception or
							signal.is_segmentation_fault or signal.is_bus_error or
							signal.is_trace
		do
			Result := c_get_si_addr_lsb(item)
		end

	exit_value:INTEGER assign set_exit_value
			-- The exit value of the child process of `Current'
			-- (Only used in SIGCHLD)
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
			-- ToDo: code = CLD_EXITED
		do
			Result := c_get_si_status(item)
		end


feature -- Status setting

	set_signal(a_signal:PTRACE_SIGNAL_READABLE)
			-- Assign `signal' with the value of `a_signal'
		require
			Exists: exists
		do
			c_set_si_signo(item, a_signal.item)
		ensure
			Is_Assign: signal ~ a_signal
		end

	set_error_number(a_number:INTEGER)
			-- Assign `error_number' with the value of `a_number'
		require
			Exists: exists
		do
			c_set_si_errno(item, a_number)
		ensure
			Is_Assign: error_number ~ a_number
		end

	set_code(a_code:INTEGER)
			-- Assign `code' with the value of `a_code'
		require
			Exists: exists
		do
			c_set_si_code(item, a_code)
		ensure
			Is_Assign: code ~ a_code
		end

	set_process_id(a_process_id:INTEGER)
			-- Assign `process_id' with the value of `a_process_id'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			c_set_si_pid(item, a_process_id)
		ensure
			Is_Assign: process_id ~ a_process_id
		end

	set_user_id(a_user_id:INTEGER)
			-- Assign `user_id' with the value of `a_user_id'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			c_set_si_uid(item, a_user_id)
		ensure
			Is_Assign: user_id ~ a_user_id
		end

	set_user_time_consumed(a_time:INTEGER)
			-- Assign `user_time_consumed' with the value of `a_time'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			c_set_si_utime(item, a_time)
		ensure
			Is_Assign: user_time_consumed ~ a_time
		end

	set_system_time_consumed(a_time:INTEGER)
			-- Assign `system_time_consumed' with the value of `a_time'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
		do
			c_set_si_stime(item, a_time)
		ensure
			Is_Assign: system_time_consumed ~ a_time
		end

	set_value(a_value:INTEGER)
			-- Assign `value' with the value of `a_value'
		require
			Exists: exists
		do
			c_set_si_value_int(item, a_value)
		ensure
			Is_Assign: value ~ a_value
		end

	set_value_pointer(a_value:POINTER)
			-- Assign `value_pointer' with the value of `a_value'
		require
			Exists: exists
		do
			c_set_si_value_pointer(item, a_value)
		ensure
			Is_Assign: value_pointer ~ a_value
		end

	set_posix_1b_value(a_value:INTEGER)
			-- Assign `posix_1b_value' with the value of `a_value'
		require
			Exists: exists
		do
			c_set_si_int(item, a_value)
		ensure
			Is_Assign: posix_1b_value ~ a_value
		end

	set_posix_1b_value_pointer(a_value:POINTER)
			-- Assign `posix_1b_value_pointer' with the value of `a_value'
		require
			Exists: exists
		do
			c_set_si_ptr(item, a_value)
		ensure
			Is_Assign: posix_1b_value_pointer ~ a_value
		end

	set_overrun_count(a_count:INTEGER)
			-- Assign `overrun_count' with the value of `a_count'
		require
			Exists: exists
		do
			c_set_si_overrun(item, a_count)
		ensure
			Is_Assign: overrun_count ~ a_count
		end

	set_timer_id(a_id:INTEGER)
			-- Assign `timer_id' with the value of `a_id'
		require
			Exists: exists
		do
			c_set_si_timerid(item, a_id)
		ensure
			Is_Assign: timer_id ~ a_id
		end

	set_memory_fault_address(a_address:POINTER)
			-- Assign `memory_fault_address' with the value of `a_address'
		require
			Exists: exists
			Is_Signal_Valid: signal.is_kill or signal.is_floating_point_exception or
							signal.is_segmentation_fault or signal.is_bus_error or
							signal.is_trace
		do
			c_set_si_addr(item, a_address)
		ensure
			Is_Assign: memory_fault_address ~ a_address
		end

	set_band_event(a_band_event:INTEGER)
			-- Assign `band_event' with the value of `a_band_event'
		require
			Exists: exists
			Is_Signal_Valid: signal.is_io or signal.is_pollable
		do
			c_set_si_band(item, a_band_event)
		ensure
			Is_Assign: band_event ~ a_band_event
		end

	set_file_descriptor(a_file_descriptor:INTEGER)
			-- Assign `file_descriptor' with the value of `a_file_descriptor'
		require
			Exists: exists
			Is_Signal_Valid: signal.is_io or signal.is_pollable
		do
			c_set_si_fd(item, a_file_descriptor)
		ensure
			Is_Assign: file_descriptor ~ a_file_descriptor
		end

	set_memory_fault_least_significant_bits(a_bits:INTEGER_16)
			-- Assign `memory_fault_least_significant_bits' with the value of `a_bits'
		require
			Exists: exists
			Is_Signal_Valid: signal.is_kill or signal.is_floating_point_exception or
							signal.is_segmentation_fault or signal.is_bus_error or
							signal.is_trace
		do
			c_set_si_addr_lsb(item, a_bits)
		ensure
			Is_Assign: memory_fault_least_significant_bits ~ a_bits
		end

	set_exit_value(a_exit_value:INTEGER)
			-- Assign `exit_value' with the value of `a_exit_value'
		require
			Exists: exists
			Is_Child_Stop_Signal: signal.is_child_stop
			-- ToDo code = CLD_EXITED
		do
			c_set_si_status(item, a_exit_value)
		ensure
			Is_Assign: exit_value ~ a_exit_value
		end

feature {NONE} -- Implementation

	dispose
			-- <Precursor>
		do
			item.memory_free
		end

feature {PTRACE} -- External

	c_sizeof_siginfo_t:INTEGER
			-- The size of a siginfo_t C structure
		external
			"C inline use <signal.h>"
		alias
			"sizeof(siginfo_t)"
		end

	item:POINTER
			-- Internal pointeur of `Current'

feature {NONE} -- Externals

	c_get_si_signo(a_item:POINTER):INTEGER
			-- Access the signal number
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_signo"
		end


	c_set_si_signo(a_item:POINTER; a_value:INTEGER)
			-- Assign the signal number
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_signo = $a_value"
		ensure
			Is_Assign: c_get_si_signo(a_item) ~ a_value
		end

	c_get_si_errno(a_item:POINTER):INTEGER
			-- Access the error number value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_errno"
		end


	c_set_si_errno(a_item:POINTER; a_value:INTEGER)
			-- Assign the error number value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_errno = $a_value"
		ensure
			Is_Assign: c_get_si_errno(a_item) ~ a_value
		end

	c_get_si_code(a_item:POINTER):INTEGER
			-- Access the signal code
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_code"
		end


	c_set_si_code(a_item:POINTER; a_value:INTEGER)
			-- Assign the signal code
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_code = $a_value"
		ensure
			Is_Assign: c_get_si_code(a_item) ~ a_value
		end

	c_get_si_pid(a_item:POINTER):INTEGER
			-- Access the sending process ID
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_pid"
		end


	c_set_si_pid(a_item:POINTER; a_value:INTEGER)
			-- Assign the sending process ID
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_pid = $a_value"
		ensure
			Is_Assign: c_get_si_pid(a_item) ~ a_value
		end

	c_get_si_uid(a_item:POINTER):INTEGER
			-- Access the real user ID of sending process
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_uid"
		end


	c_set_si_uid(a_item:POINTER; a_value:INTEGER)
			-- Assign the real user ID of sending process
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_uid = $a_value"
		ensure
			Is_Assign: c_get_si_uid(a_item) ~ a_value
		end

	c_get_si_utime(a_item:POINTER):INTEGER
			-- Access the user time consumed
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_utime"
		end


	c_set_si_utime(a_item:POINTER; a_value:INTEGER)
			-- Assign the user time consumed
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_utime = $a_value"
		ensure
			Is_Assign: c_get_si_utime(a_item) ~ a_value
		end

	c_get_si_stime(a_item:POINTER):INTEGER
			-- Access the system time consumed
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_stime"
		end


	c_set_si_stime(a_item:POINTER; a_value:INTEGER)
			-- Assign the system time consumed
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_stime = $a_value"
		ensure
			Is_Assign: c_get_si_stime(a_item) ~ a_value
		end

	c_get_si_value_int(a_item:POINTER):INTEGER
			-- Access the signal int value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_value.sival_int"
		end


	c_set_si_value_int(a_item:POINTER; a_value:INTEGER)
			-- Assign the signal int value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_value.sival_int = $a_value"
		ensure
			Is_Assign: c_get_si_value_int(a_item) ~ a_value
		end

	c_get_si_value_pointer(a_item:POINTER):POINTER
			-- Access the signal pointer value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_value.sival_ptr"
		end


	c_set_si_value_pointer(a_item:POINTER; a_value:POINTER)
			-- Assign the signal pointer value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_value.sival_ptr = $a_value"
		ensure
			Is_Assign: c_get_si_value_pointer(a_item) ~ a_value
		end

	c_get_si_int(a_item:POINTER):INTEGER
			-- Access the POSIX.1b signal integer value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_int"
		end


	c_set_si_int(a_item:POINTER; a_value:INTEGER)
			-- Assign the POSIX.1b signal integer value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_int = $a_value"
		ensure
			Is_Assign: c_get_si_int(a_item) ~ a_value
		end

	c_get_si_ptr(a_item:POINTER):POINTER
			-- Access the POSIX.1b signal pointer value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_ptr"
		end


	c_set_si_ptr(a_item:POINTER; a_value:POINTER)
			-- Assign the POSIX.1b signal pointer value
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_ptr = $a_value"
		ensure
			Is_Assign: c_get_si_ptr(a_item) ~ a_value
		end

	c_get_si_overrun(a_item:POINTER):INTEGER
			-- Access the timer overrun count for POSIX.1b timers
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_overrun"
		end


	c_set_si_overrun(a_item:POINTER; a_value:INTEGER)
			-- Assign the timer overrun count for POSIX.1b timers
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_overrun = $a_value"
		ensure
			Is_Assign: c_get_si_overrun(a_item) ~ a_value
		end

	c_get_si_timerid(a_item:POINTER):INTEGER
			-- Access the timer ID for POSIX.1b timers
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_timerid"
		end


	c_set_si_timerid(a_item:POINTER; a_value:INTEGER)
			-- Assign the timer ID for POSIX.1b timers
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_timerid = $a_value"
		ensure
			Is_Assign: c_get_si_timerid(a_item) ~ a_value
		end

	c_get_si_addr(a_item:POINTER):POINTER
			-- Access the memory location which caused fault
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_addr"
		end


	c_set_si_addr(a_item:POINTER; a_value:POINTER)
			-- Assign the memory location which caused fault
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_addr = $a_value"
		ensure
			Is_Assign: c_get_si_addr(a_item) ~ a_value
		end

	c_get_si_band(a_item:POINTER):INTEGER
			-- Access the band event
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_band"
		end


	c_set_si_band(a_item:POINTER; a_value:INTEGER)
			-- Assign the band event
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_band = $a_value"
		ensure
			Is_Assign: c_get_si_band(a_item) ~ a_value
		end

	c_get_si_fd(a_item:POINTER):INTEGER
			-- Access the file descriptor
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_fd"
		end


	c_set_si_fd(a_item:POINTER; a_value:INTEGER)
			-- Assign the file descriptor
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_fd = $a_value"
		ensure
			Is_Assign: c_get_si_fd(a_item) ~ a_value
		end

	c_get_si_addr_lsb(a_item:POINTER):INTEGER_16
			-- Access the least significant bit of address
			-- (since Linux kernel 2.6.32)
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_addr_lsb"
		end


	c_set_si_addr_lsb(a_item:POINTER; a_value:INTEGER_16)
			-- Assign the least significant bit of address
			-- (since Linux kernel 2.6.32)
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_addr_lsb = $a_value"
		ensure
			Is_Assign: c_get_si_addr_lsb(a_item) ~ a_value
		end

	c_get_si_status(a_item:POINTER):INTEGER
			-- Access the exit value or signal status
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_status"
		end


	c_set_si_status(a_item:POINTER; a_value:INTEGER)
			-- Assign the exit value or signal status
		require
			Item_Exists: not a_item.is_default_pointer
		external
			"C inline use <signal.h>"
		alias
			"((siginfo_t *)$a_item)->si_status = $a_value"
		ensure
			Is_Assign: c_get_si_status(a_item) ~ a_value
		end


end
