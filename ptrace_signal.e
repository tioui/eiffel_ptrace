note
	description: "A signal that can be modified."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"

class
	PTRACE_SIGNAL

inherit
	PTRACE_SIGNAL_READABLE

create {PTRACE}
	make_from_status

create {PTRACE, PTRACE_SIGNAL_INFORMATION}
	make

create
	make_from_other,
	make_abort,
	make_timer,
	make_bus_error,
	make_child_stop,
	make_continue,
	make_floating_point_exception,
	make_hangup,
	make_illegal_instruction,
	make_power_failure,
	make_keyboard_interrupt,
	make_io,
	make_iot,
	make_kill,
	make_broken_pipe,
	make_pollable,
	make_profiling_timer_expired,
	make_quit,
	make_segmentation_fault,
	make_stack_fault,
	make_stop,
	make_terminal_stop,
	make_bad_system_call,
	make_termination,
	make_trace,
	make_terminal_input_baground,
	make_terminal_output_baground,
	make_urgent_condition_on_socket,
	make_user1,
	make_user2,
	make_virtual_alarm_clock,
	make_cpu_time_limit_exceeded,
	make_file_size_limit_exceeded,
	make_window_resized

feature {NONE} -- Initialisation

	make_abort
			-- Initialisation of `Current' as an abort signal
		do
			set_abort
		ensure
			Is_abort: is_abort
		end

	make_timer
			-- Initialisation of `Current' as a timer signal
		do
			set_timer
		ensure
			Is_timer: is_timer
		end

	make_bus_error
			-- Initialisation of `Current' as a bus error signal
		do
			set_bus_error
		ensure
			Is_bus_error: is_bus_error
		end

	make_child_stop
			-- Initialisation of `Current' as a Child stopped or terminated signal
		do
			set_child_stop
		ensure
			Is_child_stop: is_child_stop
		end

	make_continue
			-- Initialisation of `Current' as a continue if stopped signal
		do
			set_continue
		ensure
			Is_continue: is_continue
		end

	make_floating_point_exception
			-- Initialisation of `Current' as a floating point exception signal
		do
			set_floating_point_exception
		ensure
			Is_floating_point_exception: is_floating_point_exception
		end

	make_hangup
			-- Initialisation of `Current' as a hangup (or death of controlling process) signal
		do
			set_hangup
		ensure
			Is_hangup: is_hangup
		end

	make_illegal_instruction
			-- Initialisation of `Current' as an illegal instruction signal
		do
			set_illegal_instruction
		ensure
			Is_illegal_instruction: is_illegal_instruction
		end

	make_power_failure
			-- Initialisation of `Current' as an illegal instruction signal (System V)
		do
			set_power_failure
		ensure
			Is_power_failure: is_power_failure
		end

	make_keyboard_interrupt
			-- Initialisation of `Current' as a keyboard interrupt signal
		do
			set_keyboard_interrupt
		ensure
			Is_keyboard_interrupt: is_keyboard_interrupt
		end

	make_io
			-- Initialisation of `Current' as I/O now possible signal (4.2BSD)
		do
			set_io
		ensure
			Is_io: is_io
		end

	make_iot
			-- Initialisation of `Current' as an IOT trap
		do
			set_iot
		ensure
			Is_iot: is_iot
		end

	make_kill
			-- Initialisation of `Current' as a kill signal
		do
			set_kill
		ensure
			Is_kill: is_kill
		end

	make_broken_pipe
			-- Initialisation of `Current' as a broken pipe signal
		do
			set_broken_pipe
		ensure
			Is_broken_pipe: is_broken_pipe
		end

	make_pollable
			-- Initialisation of `Current' as a pollable event (System V)
		do
			set_pollable
		ensure
			Is_pollable: is_pollable
		end

	make_profiling_timer_expired
			-- Initialisation of `Current' as a profiling timer expired event
		do
			set_profiling_timer_expired
		ensure
			Is_profiling_timer_expired: is_profiling_timer_expired
		end

	make_quit
			-- Initialisation of `Current' as a quit from keyboard signal
		do
			set_quit
		ensure
			Is_quit: is_quit
		end

	make_segmentation_fault
			-- Initialisation of `Current' as an invalid memory reference signal
		do
			set_segmentation_fault
		ensure
			Is_segmentation_fault: is_segmentation_fault
		end

	make_stack_fault
			-- Initialisation of `Current' as an stack fault on coprocessor signal (unused)
		do
			set_stack_fault
		ensure
			Is_stack_fault: is_stack_fault
		end

	make_stop
			-- Initialisation of `Current' as a stop process signal
		do
			set_stop
		ensure
			Is_stop: is_stop
		end

	make_terminal_stop
			-- Initialisation of `Current' as a stop typed at terminals signal
		do
			set_terminal_stop
		ensure
			Is_terminal_stop: is_terminal_stop
		end

	make_bad_system_call
			-- Initialisation of `Current' as a bad system call signal (SVr4)
		do
			set_bad_system_call
		ensure
			Is_bad_system_call: is_bad_system_call
		end

	make_termination
			-- Initialisation of `Current' as a termination signal
		do
			set_termination
		ensure
			Is_termination: is_termination
		end

	make_trace
			-- Initialisation of `Current' as a trace/breakpoint trap signal
		do
			set_trace
		ensure
			Is_trace: is_trace
		end

	make_terminal_input_baground
			-- Initialisation of `Current' as a terminal input for background process signal
		do
			set_terminal_input_baground
		ensure
			Is_terminal_input_baground: is_terminal_input_baground
		end

	make_terminal_output_baground
			-- Initialisation of `Current' as a terminal output for background process signal
		do
			set_terminal_output_baground
		ensure
			Is_terminal_output_baground: is_terminal_output_baground
		end

	make_urgent_condition_on_socket
			-- Initialisation of `Current' as an urgent condition on socket signal (4.2BSD)
		do
			set_urgent_condition_on_socket
		ensure
			Is_urgent_condition_on_socket: is_urgent_condition_on_socket
		end

	make_user1
			-- Initialisation of `Current' as a first user-defined signal
		do
			set_user1
		ensure
			Is_user1: is_user1
		end

	make_user2
			-- Initialisation of `Current' as a second user-defined signall
		do
			set_user2
		ensure
			Is_user2: is_user2
		end

	make_virtual_alarm_clock
			-- Initialisation of `Current' as a virtual alarm clock signal (4.2BSD)
		do
			set_virtual_alarm_clock
		ensure
			Is_virtual_alarm_clock: is_virtual_alarm_clock
		end

	make_cpu_time_limit_exceeded
			-- Initialisation of `Current' as a CPU time limit exceeded signal (4.2BSD)
		do
			set_cpu_time_limit_exceeded
		ensure
			Is_cpu_time_limit_exceeded: is_cpu_time_limit_exceeded
		end

	make_file_size_limit_exceeded
			-- Initialisation of `Current' as a file size limit exceeded signal (4.2BSD)
		do
			set_file_size_limit_exceeded
		ensure
			Is_file_size_limit_exceeded: is_file_size_limit_exceeded
		end

	make_window_resized
			-- Initialisation of `Current' as a Window resize signal (4.3BSD, Sun)
		do
			set_window_resized
		ensure
			Is_window_resized: is_window_resized
		end

feature -- Status setting

	set_abort
			-- Set `Current' as an abort signal
		do
			item := c_sigabrt
		ensure
			Is_abort: is_abort
		end

	set_timer
			-- Set `Current' as a timer signal
		do
			item := c_sigalrm
		ensure
			Is_timer: is_timer
		end

	set_bus_error
			-- Set `Current' as a bus error signal
		do
			item := c_sigbus
		ensure
			Is_bus_error: is_bus_error
		end

	set_child_stop
			-- Set `Current' as a Child stopped or terminated signal
		do
			item := c_sigchld
		ensure
			Is_child_stop: is_child_stop
		end

	set_continue
			-- Set `Current' as a continue if stopped signal
		do
			item := c_sigcont
		ensure
			Is_continue: is_continue
		end

	set_floating_point_exception
			-- Set `Current' as a floating point exception signal
		do
			item := c_sigfpe
		ensure
			Is_floating_point_exception: is_floating_point_exception
		end

	set_hangup
			-- Set `Current' as a hangup (or death of controlling process) signal
		do
			item := c_sighup
		ensure
			Is_hangup: is_hangup
		end

	set_illegal_instruction
			-- Set `Current' as an illegal instruction signal
		do
			item := c_sigill
		ensure
			Is_illegal_instruction: is_illegal_instruction
		end

	set_power_failure
			-- Set `Current' as an illegal instruction signal (System V)
		do
			item := c_sigpwr
		ensure
			Is_power_failure: is_power_failure
		end

	set_keyboard_interrupt
			-- Set `Current' as a keyboard interrupt signal
		do
			item := c_sigint
		ensure
			Is_keyboard_interrupt: is_keyboard_interrupt
		end

	set_io
			-- Set `Current' as I/O now possible signal (4.2BSD)
		do
			item := c_sigio
		ensure
			Is_io: is_io
		end

	set_iot
			-- Set `Current' as an IOT trap
		do
			item := c_sigiot
		ensure
			Is_iot: is_iot
		end

	set_kill
			-- Set `Current' as a kill signal
		do
			item := c_sigkill
		ensure
			Is_kill: is_kill
		end

	set_broken_pipe
			-- Set `Current' as a broken pipe signal
		do
			item := c_sigpipe
		ensure
			Is_broken_pipe: is_broken_pipe
		end

	set_pollable
			-- Set `Current' as a pollable event (System V)
		do
			item := c_sigpoll
		ensure
			Is_pollable: is_pollable
		end

	set_profiling_timer_expired
			-- Set `Current' as a profiling timer expired event
		do
			item := c_sigprof
		ensure
			Is_profiling_timer_expired: is_profiling_timer_expired
		end

	set_quit
			-- Set `Current' as a quit from keyboard signal
		do
			item := c_sigquit
		ensure
			Is_quit: is_quit
		end

	set_segmentation_fault
			-- Set `Current' as an invalid memory reference signal
		do
			item := c_sigsegv
		ensure
			Is_segmentation_fault: is_segmentation_fault
		end

	set_stack_fault
			-- Set `Current' as an stack fault on coprocessor signal (unused)
		do
			item := c_sigstkflt
		ensure
			Is_stack_fault: is_stack_fault
		end

	set_stop
			-- Set `Current' as a stop process signal
		do
			item := c_sigstop
		ensure
			Is_stop: is_stop
		end

	set_terminal_stop
			-- Set `Current' as a stop typed at terminals signal
		do
			item := c_sigtstp
		ensure
			Is_terminal_stop: is_terminal_stop
		end

	set_bad_system_call
			-- Set `Current' as a bad system call signal (SVr4)
		do
			item := c_sigsys
		ensure
			Is_bad_system_call: is_bad_system_call
		end

	set_termination
			-- Set `Current' as a termination signal
		do
			item := c_sigterm
		ensure
			Is_termination: is_termination
		end

	set_trace
			-- Set `Current' as a trace/breakpoint trap signal
		do
			item := c_sigtrap
		ensure
			Is_trace: is_trace
		end

	set_terminal_input_baground
			-- Set `Current' as a terminal input for background process signal
		do
			item := c_sigttin
		ensure
			Is_terminal_input_baground: is_terminal_input_baground
		end

	set_terminal_output_baground
			-- Set `Current' as a terminal output for background process signal
		do
			item := c_sigttou
		ensure
			Is_terminal_output_baground: is_terminal_output_baground
		end

	set_urgent_condition_on_socket
			-- Set `Current' as an urgent condition on socket signal (4.2BSD)
		do
			item := c_sigttou
		ensure
			Is_urgent_condition_on_socket: is_urgent_condition_on_socket
		end

	set_user1
			-- Set `Current' as a first user-defined signal
		do
			item := c_sigusr1
		ensure
			Is_user1: is_user1
		end

	set_user2
			-- Set `Current' as a second user-defined signall
		do
			item := c_sigusr2
		ensure
			Is_user2: is_user2
		end

	set_virtual_alarm_clock
			-- Set `Current' as a virtual alarm clock signal (4.2BSD)
		do
			item := c_sigvtalrm
		ensure
			Is_virtual_alarm_clock: is_virtual_alarm_clock
		end

	set_cpu_time_limit_exceeded
			-- Set `Current' as a CPU time limit exceeded signal (4.2BSD)
		do
			item := c_sigxcpu
		ensure
			Is_cpu_time_limit_exceeded: is_cpu_time_limit_exceeded
		end

	set_file_size_limit_exceeded
			-- Set `Current' as a file size limit exceeded signal (4.2BSD)
		do
			item := c_sigxfsz
		ensure
			Is_file_size_limit_exceeded: is_file_size_limit_exceeded
		end

	set_window_resized
			-- Set `Current' as a Window resize signal (4.3BSD, Sun)
		do
			item := c_sigxfsz
		ensure
			Is_window_resized: is_window_resized
		end


end
