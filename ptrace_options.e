note
	description: "Options used in a {PTRACE}."
	Author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 05 Jan 2022 18:44:30 +0000"
	revision:    "0.1"
	ToDo:        "Make the links with PTRACE_GETEVENTMSG in option documentations."

class
	PTRACE_OPTIONS

inherit
	ANY
		redefine
			default_create, copy, is_equal
		end

create
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			internal_value := 0
		end

feature -- Access

	has_kill_on_exit:BOOLEAN assign set_has_kill_on_exit
			-- If this option is set, a SIGKILL signal will be sent to every tracee
			-- if the tracer exits. This option is useful for ptrace jailers that
			-- want to ensure that tracees can never escape the tracer's control.
		do
			Result := internal_value.bit_and (c_ptrace_o_exitkill) /= 0
		end

	has_trace_clone:BOOLEAN assign set_has_trace_clone
			-- If this option is set, stop the tracee at the next clone and
			-- automatically start tracing the newly cloned process, which
			-- will start with a SIGSTOP.
		do
			Result := internal_value.bit_and (c_ptrace_o_traceclone) /= 0
		end

	has_trace_exec:BOOLEAN assign set_has_trace_exec
			-- If this option is set, stop the tracee at the next exec (or variant)
		do
			Result := internal_value.bit_and (c_ptrace_o_traceexec) /= 0
		end

	has_trace_exit:BOOLEAN assign set_has_trace_exit
			-- If this option is set, stop the tracee when it exit. This allow the tracer to see
			-- where the exit occurred
		do
			Result := internal_value.bit_and (c_ptrace_o_traceexit) /= 0
		end

	has_trace_fork:BOOLEAN assign set_has_trace_fork
			-- If this option is set, stop the tracee at the next fork and automatically
			-- start tracing the newly forked process, which will start with a SIGSTOP.
		do
			Result := internal_value.bit_and (c_ptrace_o_tracefork) /= 0
		end

	has_trace_vfork:BOOLEAN assign set_has_trace_vfork
			-- If this option is set, stop the tracee at the next vfork and automatically
			-- start tracing the newly vforked process, which will start with a SIGSTOP.
		do
			Result := internal_value.bit_and (c_ptrace_o_tracevfork) /= 0
		end

	has_trace_vfork_completion:BOOLEAN assign set_has_trace_vfork_completion
			-- If this option is set, stop the tracee at the completion of the next vfork.
		do
			Result := internal_value.bit_and (c_ptrace_o_tracevforkdone) /= 0
		end

	has_bit_7_at_system_trap:BOOLEAN assign set_has_bit_7_at_system_trap
			-- If this option is set, when delivering system call traps,
			-- set bit 7 in the signal number (i.e., deliver SIGTRAP|0x80).
			-- This makes it easy for the tracer to distinguish normal traps
			-- from those caused by a system call.
			-- Note that this may not work on every architecture.
		do
			Result := internal_value.bit_and (c_ptrace_o_tracesysgood) /= 0
		end

feature -- Setting

	set_has_kill_on_exit(a_value:BOOLEAN)
			-- Assign `has_kill_on_exit' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_exitkill)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_exitkill.bit_not)
			end
		ensure
			Is_Assign: has_kill_on_exit = a_value
		end

	enable_kill_on_exit
			-- Set `has_kill_on_exit' to `True'
		do
			set_has_kill_on_exit(True)
		ensure
			Is_Set: has_kill_on_exit
		end

	disable_kill_on_exit
			-- Set `has_kill_on_exit' to `False'
		do
			set_has_kill_on_exit(False)
		ensure
			Is_Unset: not has_kill_on_exit
		end

	set_has_trace_clone(a_value:BOOLEAN)
			-- Assign `has_trace_clone' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_traceclone)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_traceclone.bit_not)
			end
		ensure
			Is_Assign: has_trace_clone = a_value
		end

	enable_trace_clone
			-- Set `has_trace_clone' to `True'
		do
			set_has_trace_clone(True)
		ensure
			Is_Set: has_trace_clone
		end

	disable_trace_clone
			-- Set `has_trace_clone' to `False'
		do
			set_has_trace_clone(False)
		ensure
			Is_Unset: not has_trace_clone
		end

	set_has_trace_exec(a_value:BOOLEAN)
			-- Assign `has_trace_exec' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_traceexec)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_traceexec.bit_not)
			end
		ensure
			Is_Assign: has_trace_exec = a_value
		end

	enable_trace_exec
			-- Set `has_trace_exec' to `True'
		do
			set_has_trace_exec(True)
		ensure
			Is_Set: has_trace_exec
		end

	disable_trace_exec
			-- Set `has_trace_exec' to `False'
		do
			set_has_trace_exec(False)
		ensure
			Is_Unset: not has_trace_exec
		end

	set_has_trace_exit(a_value:BOOLEAN)
			-- Assign `has_trace_exit' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_traceexit)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_traceexit.bit_not)
			end
		ensure
			Is_Assign: has_trace_exit = a_value
		end

	enable_trace_exit
			-- Set `has_trace_exit' to `True'
		do
			set_has_trace_exit(True)
		ensure
			Is_Set: has_trace_exit
		end

	disable_trace_exit
			-- Set `has_trace_exit' to `False'
		do
			set_has_trace_exit(False)
		ensure
			Is_Unset: not has_trace_exit
		end

	set_has_trace_fork(a_value:BOOLEAN)
			-- Assign `has_trace_fork' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_tracefork)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_tracefork.bit_not)
			end
		ensure
			Is_Assign: has_trace_fork = a_value
		end

	enable_trace_fork
			-- Set `has_trace_fork' to `True'
		do
			set_has_trace_fork(True)
		ensure
			Is_Set: has_trace_fork
		end

	disable_trace_fork
			-- Set `has_trace_fork' to `False'
		do
			set_has_trace_fork(False)
		ensure
			Is_Unset: not has_trace_fork
		end

	set_has_trace_vfork(a_value:BOOLEAN)
			-- Assign `has_trace_vfork' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_tracevfork)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_tracevfork.bit_not)
			end
		ensure
			Is_Assign: has_trace_vfork = a_value
		end

	enable_trace_vfork
			-- Set `has_trace_vfork' to `True'
		do
			set_has_trace_vfork(True)
		ensure
			Is_Set: has_trace_vfork
		end

	disable_trace_vfork
			-- Set `has_trace_vfork' to `False'
		do
			set_has_trace_vfork(False)
		ensure
			Is_Unset: not has_trace_vfork
		end

	set_has_trace_vfork_completion(a_value:BOOLEAN)
			-- Assign `has_trace_vfork_completion' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_tracevforkdone)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_tracevforkdone.bit_not)
			end
		ensure
			Is_Assign: has_trace_vfork_completion = a_value
		end

	enable_trace_vfork_completion
			-- Set `has_trace_vfork_completion' to `True'
		do
			set_has_trace_vfork_completion(True)
		ensure
			Is_Set: has_trace_vfork_completion
		end

	disable_trace_vfork_completion
			-- Set `has_trace_vfork_completion' to `False'
		do
			set_has_trace_vfork_completion(False)
		ensure
			Is_Unset: not has_trace_vfork_completion
		end

	set_has_bit_7_at_system_trap(a_value:BOOLEAN)
			-- Assign `has_bit_7_at_system_trap' with the value `a_value'
		do
			if a_value then
				internal_value := internal_value.bit_or (c_ptrace_o_tracesysgood)
			else
				internal_value := internal_value.bit_and (c_ptrace_o_tracesysgood.bit_not)
			end
		ensure
			Is_Assign: has_bit_7_at_system_trap = a_value
		end

	enable_bit_7_at_system_trap
			-- Set `has_bit_7_at_system_trap' to `True'
		do
			set_has_bit_7_at_system_trap(True)
		ensure
			Is_Set: has_bit_7_at_system_trap
		end

	disable_bit_7_at_system_trap
			-- Set `has_bit_7_at_system_trap' to `False'
		do
			set_has_bit_7_at_system_trap(False)
		ensure
			Is_Unset: not has_bit_7_at_system_trap
		end



feature -- Comparison

	is_equal (a_other: like Current): BOOLEAN
			-- Is `a_other' attached to an object considered
			-- equal to current object?
		do
			Result := internal_value ~ a_other.internal_value
		end

feature -- Duplication

	copy (a_other: like Current)
			-- Update current object using fields of object attached
			-- to `a_other`, so as to yield equal objects.
		do
			internal_value := a_other.internal_value
		end

feature {PTRACE, PTRACE_OPTIONS} -- Implementation

	internal_value:INTEGER
			-- Internal representation of `Current'

feature {NONE} -- External

	c_ptrace_o_exitkill:INTEGER
			-- Internal value of `has_kill_on_exit'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_EXITKILL"
		end

	c_ptrace_o_traceclone:INTEGER
			-- Internal value of `has_trace_clone'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACECLONE"
		end

	c_ptrace_o_traceexec:INTEGER
			-- Internal value of `has_trace_exec'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACEEXEC"
		end

	c_ptrace_o_traceexit:INTEGER
			-- Internal value of `has_trace_exit'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACEEXIT"
		end

	c_ptrace_o_tracefork:INTEGER
			-- Internal value of `has_trace_fork'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACEFORK"
		end

	c_ptrace_o_tracesysgood:INTEGER
			-- Internal value of `has_bit_7_at_system_trap'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACESYSGOOD"
		end

	c_ptrace_o_tracevfork:INTEGER
			-- Internal value of `has_trace_vfork'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACEVFORK"
		end

	c_ptrace_o_tracevforkdone:INTEGER
			-- Internal value of `has_trace_vfork_completion'
		external
			"C inline use <sys/ptrace.h>"
		alias
			"PTRACE_O_TRACEVFORKDONE"
		end




end
