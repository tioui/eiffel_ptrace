note
	description: "[
					Every registers usable in the system.

					Note that if you see this class in your project, you did not
					initialise the OS_PLATFORM environment variable correctly
					or you are on an unsuported platform.

					To initialise the OS_PLATFORM use the bash command before compiling
					the project:

						export OS_PLATFORM=`uname -s -m`

					To be adapted if you don't use bash as shell interpreter.
				]"
	author:      "Louis Marchand"
	license:     "MIT License"
	date:        "Wed, 11 May 2022 14:27:49 +0000"
	revision:    "0.1"

class
	PTRACE_REGISTER

inherit
	ANY
		redefine
			default_create
		end

create {PTRACE}
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			item := c_initialisation
		end


feature -- Access

	exists:BOOLEAN
		do
			Result := not item.is_default_pointer
		end

feature {NONE} -- C Externals

	c_initialisation:POINTER
			-- Initialise a 'user_regs_struct' structure
		external
			"C inline use <sys/user.h>"
		alias
			"calloc(1, sizeof(struct user_regs_struct))"
		end

feature {PTRACE} -- Implementation

	item:POINTER
			-- The internal pointeur of `Current'

end
